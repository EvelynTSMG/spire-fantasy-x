package SpireFantasyX.actions;

import com.megacrit.cardcrawl.actions.AbstractGameAction;

import java.util.ArrayList;
import java.util.Arrays;

import static SpireFantasyX.util.Wiz.att;

public class SequenceAction extends AbstractGameAction {
    public final ArrayList<AbstractGameAction> actions;

    public SequenceAction(AbstractGameAction... actions) {
        this.actions = new ArrayList<>(Arrays.asList(actions));
    }

    @Override
    public void update() {
        for (int i = actions.size() - 1; i >= 0; i--) {
            att(actions.get(i));
        }
        isDone = true;
    }
}
