package SpireFantasyX.actions;

import com.evacipated.cardcrawl.mod.stslib.patches.CenterGridCardSelectScreen;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardGroup;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Shamelessly <s>stolen</s> taken from Mistress Autumn's Starlight Sisters
 */
public class BetterSelectCardsCenteredAction extends AbstractGameAction {
    private Consumer<List<AbstractCard>> callback;
    private String text;
    private boolean anyNumber;
    private CardGroup selectGroup;
    private boolean forceSelect;

    /**
     * @param group         ArrayList of cards to filter and select from.
     * @param amount        Maximum number of cards allowed for selectoin
     * @param textForSelect Text that will be displayed on the grid select screen at the bottom. It will show just this text with nothing else added by itself.
     * @param anyNumber     Whether player has to select exact number of cards (amount) or any number up to, including 0.
     *                      false for exact number.
     * @param cardFilter    Filters the cards in the group.
     * @param callback      What to do with cards selected. Accepts a list with cards selected. The list would only contain one element if it's "Select one card"
     *                      if there's no callback the action will not trigger simply because you told player to "select cards to do nothing with them"
     *
     */

    public BetterSelectCardsCenteredAction(ArrayList<AbstractCard> group, int amount, String textForSelect, boolean anyNumber, Predicate<AbstractCard> cardFilter, Consumer<List<AbstractCard>> callback) {
        this.amount = amount;
        this.duration = this.startDuration = Settings.ACTION_DUR_XFAST;
        text = textForSelect;
        this.anyNumber = anyNumber;
        this.callback = callback;
        this.selectGroup = new CardGroup(CardGroup.CardGroupType.UNSPECIFIED);
        this.selectGroup.group.addAll(group.stream().distinct().filter(cardFilter).collect(Collectors.toList()));
        // It's distinct() because if I don't it may cause the infamous "jiggle" when you see a grid of cards with a same object in different locations.
    }

    public BetterSelectCardsCenteredAction(ArrayList<AbstractCard> group, String textForSelect, boolean anyNumber, Predicate<AbstractCard> cardFilter, Consumer<List<AbstractCard>> callback) {
        this(group, 1, textForSelect, anyNumber, cardFilter, callback);
    }

    public BetterSelectCardsCenteredAction(ArrayList<AbstractCard> group, String textForSelect, Predicate<AbstractCard> cardFilter, Consumer<List<AbstractCard>> callback) {
        this(group, 1, textForSelect, false, cardFilter, callback);
    }

    public BetterSelectCardsCenteredAction(ArrayList<AbstractCard> group, String textForSelect, Consumer<List<AbstractCard>> callback) {
        this(group, 1, textForSelect, false, c -> true, callback);
    }

    public BetterSelectCardsCenteredAction(ArrayList<AbstractCard> group, int amount, String textForSelect, Consumer<List<AbstractCard>> callback) {
        this(group, amount, textForSelect, false, c -> true, callback);
    }

    public BetterSelectCardsCenteredAction(ArrayList<AbstractCard> group, int amount, String textForSelect, boolean forceSelect, Consumer<List<AbstractCard>> callback) {
        this(group, amount, textForSelect, false, c -> true, callback);
        this.forceSelect = forceSelect;
    }

    public BetterSelectCardsCenteredAction(ArrayList<AbstractCard> group, int amount, String textForSelect, boolean anyNumber, boolean forceSelect, Consumer<List<AbstractCard>> callback) {
        this(group, amount, textForSelect, anyNumber, c -> true, callback);
        this.forceSelect = forceSelect;
    }

    @Override
    public void update() {
        if (this.duration == this.startDuration) {
            if ((selectGroup.size() == 0) || callback == null) {
                isDone = true;
                return;
            }

            if (!forceSelect && selectGroup.size() <= amount && !anyNumber) {
                callback.accept(selectGroup.group);
                isDone = true;
                return;
            }

            CenterGridCardSelectScreen.centerGridSelect = true;
            AbstractDungeon.gridSelectScreen.open(selectGroup, amount, anyNumber, text);
            AbstractDungeon.gridSelectScreen.forClarity = false;
            tickDuration();
        }

        if (AbstractDungeon.gridSelectScreen.selectedCards.size() != 0) {
            CenterGridCardSelectScreen.centerGridSelect = false;
            callback.accept(AbstractDungeon.gridSelectScreen.selectedCards);
            AbstractDungeon.gridSelectScreen.selectedCards.clear();
            AbstractDungeon.player.hand.refreshHandLayout();
            isDone = true;
            return;
        }
        tickDuration();
    }
}