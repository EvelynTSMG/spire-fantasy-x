package SpireFantasyX.actions;

import com.megacrit.cardcrawl.actions.AbstractGameAction;

import static SpireFantasyX.util.Wiz.player;

public class InstantAction extends AbstractGameAction {
    public final Runnable action;

    public InstantAction(Runnable action) {
        this.action = action;
        source = player();
    }

    @Override
    public void update() {
        action.run();
        isDone = true;
    }
}
