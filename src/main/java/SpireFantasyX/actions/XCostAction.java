package SpireFantasyX.actions;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.relics.ChemicalX;
import com.megacrit.cardcrawl.ui.panels.EnergyPanel;

import java.util.function.BiFunction;

public class XCostAction extends AbstractGameAction {
    public BiFunction<Integer, int[], Boolean> x_action_update;
    public int[] params;
    protected int base_value;
    protected boolean free_once;
    protected int effect;
    private boolean first_update = true;

    /**
     * @param card            The card played. Usually should simply be `this`.
     * @param x_action_update A BiFunction that receives an integer for the energy amount (includes Chem X) and any number of integer parameters in the form of an array. The return value of this function is isDone.
     * @param params          Any number of integer parameters. These will be passed to the update function to avoid possible value changes between the creation of this action and when it is updated.
     */
    public XCostAction(AbstractCard card, BiFunction<Integer, int[], Boolean> x_action_update, int... params) {
        this.base_value = card.energyOnUse;
        this.free_once = card.freeToPlayOnce;
        this.x_action_update = x_action_update;

        this.params = params;
    }

    @Override
    public void update() {
        if (first_update) {
            effect = EnergyPanel.totalCount;
            if (this.base_value != -1) {
                effect = this.base_value;
            }

            if (AbstractDungeon.player.hasRelic(ChemicalX.ID)) {
                effect += 2;
                AbstractDungeon.player.getRelic(ChemicalX.ID).flash();
            }

            isDone = x_action_update.apply(effect, params) || duration < 0.0f;
            first_update = false;

            if (!this.free_once) {
                AbstractDungeon.player.energy.use(EnergyPanel.totalCount);
            }
        } else {
            isDone = x_action_update.apply(effect, params) || duration < 0.0f;
        }
    }
}