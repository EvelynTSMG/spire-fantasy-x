package SpireFantasyX.actions;

import com.megacrit.cardcrawl.actions.AbstractGameAction;

import java.util.function.Consumer;
import java.util.function.Supplier;

import static SpireFantasyX.util.Wiz.att;

public class CompareAction<T> extends AbstractGameAction {
    private final AbstractGameAction action;
    T old;
    Supplier<T> before;
    Consumer<T> after;

    /**
     * Action that will take the '{@code old}' value returned by {@code before}, perform the {@code action},
     * and call {@code after} with the {@code old} value
     * @param action the action to perform
     * @param before the supplier of the {@code old} value
     * @param after the consumer of the {@code old} value
     */
    public CompareAction(AbstractGameAction action, Supplier<T> before, Consumer<T> after) {
        this.action = action;
        this.target = action.target;
        this.before = before;
        this.after = after;
    }

    public void update() {
        old = before.get();

        att(new CompareAction2<>(old, after));
        att(action);

        isDone = true;
    }

    private static class CompareAction2<T> extends AbstractGameAction {
        private final T old;
        Consumer<T> after;

        public CompareAction2(T old, Consumer<T> after) {
            this.old = old;
            this.after = after;
        }

        public void update() {
            isDone = true;
            after.accept(old);
        }
    }
}
