package SpireFantasyX.patches;

import SpireFantasyX.powers.interfaces.OnMonsterDeathPower;
import com.evacipated.cardcrawl.modthespire.lib.LineFinder;
import com.evacipated.cardcrawl.modthespire.lib.Matcher;
import com.evacipated.cardcrawl.modthespire.lib.SpireInsertLocator;
import com.evacipated.cardcrawl.modthespire.lib.SpireInsertPatch;
import com.evacipated.cardcrawl.modthespire.lib.SpirePatch2;
import com.evacipated.cardcrawl.modthespire.patcher.PatchingException;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.AbstractPower;
import javassist.CannotCompileException;
import javassist.CtBehavior;

@SpirePatch2(clz= AbstractMonster.class, method="die", paramtypez = { boolean.class })
public class OnMonsterDeathPowerPatch {
    @SpireInsertPatch(locator=Locator.class, localvars = { "p" })
    public static void rip(AbstractMonster __instance, boolean triggerRelics, AbstractPower p) {
        if (p instanceof OnMonsterDeathPower) ((OnMonsterDeathPower) p).on_monster_death(__instance);
    }

    private static class Locator extends SpireInsertLocator {
        public int[] Locate(CtBehavior cmtp) throws CannotCompileException, PatchingException {
            Matcher matcher = new Matcher.MethodCallMatcher(AbstractPower.class, "onDeath");
            return LineFinder.findInOrder(cmtp, matcher);
        }
    }
}
