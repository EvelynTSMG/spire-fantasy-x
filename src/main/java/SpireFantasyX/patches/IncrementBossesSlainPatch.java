package SpireFantasyX.patches;

import SpireFantasyX.SpireFantasyXMod;
import com.evacipated.cardcrawl.modthespire.lib.SpirePatch2;
import com.evacipated.cardcrawl.modthespire.lib.SpirePrefixPatch;
import com.megacrit.cardcrawl.screens.stats.CharStat;

@SpirePatch2(clz= CharStat.class, method="incrementBossSlain")
public class IncrementBossesSlainPatch {
    @SpirePrefixPatch
    public static void rip() {
        SpireFantasyXMod.enemies_killed += 1;
    }
}
