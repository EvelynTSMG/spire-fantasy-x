package SpireFantasyX.patches;

import SpireFantasyX.powers.interfaces.OnAttackPreBlockToChangeDamagePower;
import com.evacipated.cardcrawl.modthespire.lib.ByRef;
import com.evacipated.cardcrawl.modthespire.lib.LineFinder;
import com.evacipated.cardcrawl.modthespire.lib.Matcher;
import com.evacipated.cardcrawl.modthespire.lib.SpireInsertLocator;
import com.evacipated.cardcrawl.modthespire.lib.SpireInsertPatch;
import com.evacipated.cardcrawl.modthespire.lib.SpirePatch2;
import com.evacipated.cardcrawl.modthespire.patcher.PatchingException;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.AbstractPower;
import javassist.CannotCompileException;
import javassist.CtBehavior;

import static SpireFantasyX.util.Wiz.player;

@SpirePatch2(clz= AbstractMonster.class, paramtypez = { DamageInfo.class }, method = "damage")
public class OnAttackPreBlockToChangeDamagePatch {
    @SpireInsertPatch(locator = Locator.class, localvars = { "damageAmount" })
    public static void damageful(AbstractMonster __instance, DamageInfo info, @ByRef int[] damageAmount) {
        if (info.owner == player()) {
            for (AbstractPower p : info.owner.powers) {
                if (p instanceof OnAttackPreBlockToChangeDamagePower)
                    damageAmount[0] = ((OnAttackPreBlockToChangeDamagePower) p).on_attack_pre_block_to_change_damage(info, damageAmount[0]);
            }
        }
    }

    private static class Locator extends SpireInsertLocator {
        public int[] Locate(CtBehavior cmtp) throws CannotCompileException, PatchingException {
            Matcher matcher = new Matcher.MethodCallMatcher(AbstractMonster.class, "decrementBlock");
            return LineFinder.findInOrder(cmtp, matcher);
        }
    }
}
