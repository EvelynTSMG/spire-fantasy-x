package SpireFantasyX.patches;

import SpireFantasyX.cards.Zanmato;
import com.evacipated.cardcrawl.modthespire.lib.SpirePatch2;
import com.evacipated.cardcrawl.modthespire.lib.SpirePostfixPatch;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;

@SpirePatch2(clz= AbstractPlayer.class, method="gainGold")
public class ZanmatoPatch {
    @SpirePostfixPatch
    public static void live_and_learn(AbstractPlayer __instance, int amount) {
        for (AbstractCard c : __instance.masterDeck.group) {
            if (c instanceof Zanmato) c.initializeDescription();
        }

        for (AbstractCard c : __instance.discardPile.group) {
            if (c instanceof Zanmato) c.initializeDescription();
        }

        for (AbstractCard c : __instance.drawPile.group) {
            if (c instanceof Zanmato) c.initializeDescription();
        }

        for (AbstractCard c : __instance.exhaustPile.group) {
            if (c instanceof Zanmato) c.initializeDescription();
        }

        for (AbstractCard c : __instance.hand.group) {
            if (c instanceof Zanmato) c.initializeDescription();
        }

        for (AbstractCard c : __instance.limbo.group) {
            if (c instanceof Zanmato) c.initializeDescription();
        }
    }
}
