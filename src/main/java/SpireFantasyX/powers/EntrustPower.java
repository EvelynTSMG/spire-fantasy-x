package SpireFantasyX.powers;

import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

import static SpireFantasyX.util.Wiz.atb;

public class EntrustPower extends BasePower {
    public final static String NAME = EntrustPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.BUFF;
    public final static boolean TURN_BASED = false;
    public static final int DEFAULT_AMOUNT = 1;

    // Default constructor for AutoAdd
    public EntrustPower() {
        this(AbstractDungeon.player);
    }

    public EntrustPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT);
    }

    public EntrustPower(AbstractCreature owner, int amount) {
        super(NAME, TYPE, TURN_BASED, owner, amount);
        loadRegion("barricade");
    }

    @Override
    public void updateDescription() {
        this.description = String.format(DESCRIPTIONS[0], amount);
    }

    @Override
    public void onAfterCardPlayed(AbstractCard c) {
        if (c.type == AbstractCard.CardType.SKILL)
            atb(new GainBlockAction(owner, owner, amount));
    }
}