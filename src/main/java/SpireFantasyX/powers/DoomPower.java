package SpireFantasyX.powers;

import SpireFantasyX.actions.InstantAction;
import com.evacipated.cardcrawl.mod.stslib.patches.NeutralPowertypePatch;
import com.megacrit.cardcrawl.actions.common.InstantKillAction;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import java.util.Comparator;
import java.util.Optional;

import static SpireFantasyX.util.Wiz.alive_monsters;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.att;

public class DoomPower extends BasePower {
    public final static String NAME = DoomPower.class.getSimpleName();
    public final static PowerType TYPE = NeutralPowertypePatch.NEUTRAL;
    public final static boolean TURN_BASED = false;
    public static final int DEFAULT_AMOUNT = 1;

    // Default constructor for AutoAdd
    public DoomPower() {
        this(AbstractDungeon.player);
    }

    public DoomPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT);
    }

    public DoomPower(AbstractCreature owner, int amount) {
        super(NAME, TYPE, TURN_BASED, owner, amount);
        loadRegion("corruption");
    }

    @Override
    public void updateDescription() {
        this.description = String.format(DESCRIPTIONS[0], amount);
    }

    @Override
    public void atStartOfTurn() {
        amount--;
        if (amount <= 0) {
            atb(new InstantAction(() -> {
                Optional<AbstractMonster> target =
                    alive_monsters()
                        .stream()
                        .filter(m -> m.type != AbstractMonster.EnemyType.ELITE
                                  && m.type != AbstractMonster.EnemyType.BOSS)
                        .min(Comparator.comparingInt(m -> m.currentHealth));
                target.ifPresent(m -> att(new InstantKillAction(m)));
                remove_this_now();
            }));
        }
    }
}