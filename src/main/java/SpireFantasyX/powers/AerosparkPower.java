package SpireFantasyX.powers;

import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

import static SpireFantasyX.util.Wiz.att;

public class AerosparkPower extends BasePower {
    public final static String NAME = AerosparkPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.BUFF;
    public final static boolean TURN_BASED = false;
    public static final int DEFAULT_AMOUNT = 1;

    // Default constructor for AutoAdd
    public AerosparkPower() {
        this(AbstractDungeon.player);
    }

    public AerosparkPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT);
    }

    public AerosparkPower(AbstractCreature owner, int amount) {
        super(NAME, TYPE, TURN_BASED, owner, amount);
        loadRegion("mastery");
    }

    @Override
    public void updateDescription() {
        this.description = String.format(DESCRIPTIONS[0], amount);
    }

    @Override
    public void onCardDraw(AbstractCard c) {
        if (c.type == AbstractCard.CardType.SKILL) {
            att(new GainBlockAction(owner, amount));
        }
    }

    @Override
    public void atEndOfTurn(boolean isPlayer) {
        remove_this();
    }
}