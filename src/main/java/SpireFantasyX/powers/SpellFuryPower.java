package SpireFantasyX.powers;

import SpireFantasyX.cards.magic.BaseMagicCard;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.actions.utility.UseCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardQueueItem;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.util.Wiz.atb;

public class SpellFuryPower extends BasePower {
    public final static String NAME = SpellFuryPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.BUFF;
    public final static boolean TURN_BASED = false;
    public static final int DEFAULT_AMOUNT = 1;

    // Default constructor for AutoAdd
    public SpellFuryPower() {
        this(AbstractDungeon.player);
    }

    public SpellFuryPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT);
    }

    public SpellFuryPower(AbstractCreature owner, int amount) {
        super(NAME, TYPE, TURN_BASED, owner, amount);
        loadRegion("doubleTap");
    }

    @Override
    public void updateDescription() {
        this.description = String.format(DESCRIPTIONS[0], amount, amount == 1 ? DESCRIPTIONS[1] : DESCRIPTIONS[2]);
    }

    @Override
    public void onUseCard(AbstractCard card, UseCardAction action) {
        if (card.purgeOnUse || !(card instanceof BaseMagicCard) || amount <= 0) return;

        flash();
        AbstractCard tmp = card.makeSameInstanceOf();
        AbstractDungeon.player.limbo.addToBottom(tmp);
        tmp.current_x = card.current_x;
        tmp.current_y = card.current_y;
        tmp.target_x = (float) Settings.WIDTH / 2.0F - 300.0F * Settings.scale;
        tmp.target_y = (float)Settings.HEIGHT / 2.0F;
        tmp.purgeOnUse = true;
        if (action.target != null) tmp.calculateCardDamage((AbstractMonster)action.target);

        AbstractDungeon.actionManager.addCardQueueItem(
            new CardQueueItem(tmp, (AbstractMonster)action.target, card.energyOnUse, true, true),
            true);
        amount -= 1;

        if (amount <= 0)
            atb(new RemoveSpecificPowerAction(this.owner, this.owner, this));
    }

    @Override
    public void atEndOfTurn(boolean isPlayer) {
        if (!isPlayer) return;

        remove_this();
    }
}