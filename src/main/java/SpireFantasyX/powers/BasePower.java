package SpireFantasyX.powers;

import SpireFantasyX.util.TexLoader;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;

import java.util.Optional;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.SpireFantasyXMod.powerPath;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.att;

public abstract class BasePower extends AbstractPower {
    public int amount2 = 0;
    public final boolean uses_amount2;
    public static Color red = Color.RED.cpy(); // ???
    public static Color green = Color.GREEN.cpy(); // ???
    public final boolean can_be_negative = false;
    public final boolean is_amount_percent;
    public final boolean is_amount2_percent;

    public BasePower(String name, PowerType power_type, boolean turn_based, AbstractCreature owner, int amount) {
        this(name, power_type, turn_based, owner, amount, false, Optional.empty(), false);
    }

    public BasePower(String name, PowerType power_type, boolean turn_based, AbstractCreature owner,
                     int amount, boolean is_amount_percent) {
        this(name, power_type, turn_based, owner, amount, is_amount_percent, Optional.empty(), false);
    }

    public BasePower(String name, PowerType power_type, boolean turn_based, AbstractCreature owner,
                     int amount, int amount2) {
        this(name, power_type, turn_based, owner, amount, false, Optional.of(amount2), false);
    }

    public BasePower(String name, PowerType power_type, boolean turn_based, AbstractCreature owner,
                     int amount, boolean is_amount_percent,
                     int amount2, boolean is_amount2_percent) {
        this(name, power_type, turn_based, owner, amount, is_amount_percent, Optional.of(amount2), is_amount2_percent);
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private BasePower(String name, PowerType power_type, boolean turn_based, AbstractCreature owner,
                      int amount, boolean is_amount_percent,
                      Optional<Integer> amount2, boolean is_amount2_percent) {
        this.ID = id(name);

        PowerStrings POWER_STRINGS = CardCrawlGame.languagePack.getPowerStrings(this.ID);
        this.name = POWER_STRINGS.NAME;
        DESCRIPTIONS = POWER_STRINGS.DESCRIPTIONS;

        this.isTurnBased = turn_based;

        this.owner = owner;
        this.amount = amount;
        this.type = power_type;

        uses_amount2 = amount2.isPresent();
        amount2.ifPresent(x -> this.amount2 = x);

        this.is_amount_percent = is_amount_percent;
        this.is_amount2_percent = is_amount2_percent;

        Texture normal_image = TexLoader.get_texture(powerPath("lowres/" + name + ".png"));
        Texture hd_image = TexLoader.get_texture(powerPath("highres/" + name + ".png"));
        if (hd_image != null) {
            region128 = new TextureAtlas.AtlasRegion(hd_image, 0, 0, hd_image.getWidth(), hd_image.getHeight());
        }

        if (normal_image != null) {
            region48 = new TextureAtlas.AtlasRegion(normal_image, 0, 0, normal_image.getWidth(), normal_image.getHeight());
            if (hd_image == null) this.img = normal_image;
        }

        updateDescription();
    }

    public void remove_this() {
        atb(new RemoveSpecificPowerAction(owner, owner, this));
    }

    public void remove_this_now() {
        att(new RemoveSpecificPowerAction(owner, owner, this));
    }

    @Override
    public void updateDescription() {
        description = DESCRIPTIONS[0];
    }

    @Override
    public void renderAmount(SpriteBatch sb, float x, float y, Color c) {
        final String amount_text;
        if (is_amount_percent) {
            amount_text = Double.toString((double)amount / 100d) + "%";
        } else {
            amount_text = Integer.toString(amount);
        }

        if (amount > 0) {
            if (!isTurnBased) {
                green.a = c.a;
                c = green;
            }

            FontHelper.renderFontRightTopAligned(sb, FontHelper.powerAmountFont, amount_text, x, y, fontScale, c);
        } else if (this.amount < 0 && this.canGoNegative) {
            red.a = c.a;
            c = red;
            FontHelper.renderFontRightTopAligned(sb, FontHelper.powerAmountFont, amount_text, x, y, fontScale, c);
        }

        if (!uses_amount2) return;
        final String amount2_text;
        if (is_amount2_percent) {
            amount2_text = (float)amount2/100f + "%";
        } else {
            amount2_text = Integer.toString(amount2);
        }

        if (amount2 > 0) {
            if (!isTurnBased) {
                green.a = c.a;
                c = green;
            }

            FontHelper.renderFontRightTopAligned(sb, FontHelper.powerAmountFont, amount2_text, x, y + 15.0F * Settings.scale, fontScale, c);
        } else if (amount2 < 0 && can_be_negative) {
            red.a = c.a;
            c = red;
            FontHelper.renderFontRightTopAligned(sb, FontHelper.powerAmountFont, amount2_text, x, y + 15.0F * Settings.scale, fontScale, c);
        }
    }
}
