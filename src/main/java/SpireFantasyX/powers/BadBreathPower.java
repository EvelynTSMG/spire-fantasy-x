package SpireFantasyX.powers;

import basemod.AutoAdd;
import com.evacipated.cardcrawl.mod.stslib.powers.interfaces.NonStackablePower;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.AbstractPower;

import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.color_text;

@AutoAdd.Ignore
public class BadBreathPower extends BasePower implements NonStackablePower {
    public final static String NAME = BadBreathPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.BUFF;
    public final static boolean TURN_BASED = false;
    public static final int DEFAULT_AMOUNT = 1;
    public final AbstractMonster target;

    public BadBreathPower(AbstractCreature owner, AbstractMonster target) {
        this(owner, target, DEFAULT_AMOUNT);
    }

    public BadBreathPower(AbstractCreature owner, AbstractMonster target, int amount) {
        super(NAME, TYPE, TURN_BASED, owner, amount);
        this.target = target;
        loadRegion("fumes");
        updateDescription();
    }

    @Override
    public void updateDescription() {
        this.description = String.format(DESCRIPTIONS[0], amount, color_text(target != null ? target.name : "???", "#r"));
    }

    @Override
    public void atEndOfTurn(boolean isPlayer) {
        if (!isPlayer || target == null) return;

        long debuff_count = target.powers.stream().filter(pow -> pow.type == PowerType.DEBUFF).count();
        for (; debuff_count > 0; debuff_count--)
            atb(new GainBlockAction(owner, amount));
    }

    @Override
    public boolean isStackable(AbstractPower power) {
        return power instanceof BadBreathPower && ((BadBreathPower)power).target == target;
    }
}