package SpireFantasyX.powers;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageRandomEnemyAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

import static SpireFantasyX.util.Wiz.atb;

public class BoostedPower extends BasePower {
    public final static String NAME = BoostedPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.BUFF;
    public final static boolean TURN_BASED = false;
    public static final int DEFAULT_AMOUNT = 1;

    // Default constructor for AutoAdd
    public BoostedPower() {
        this(AbstractDungeon.player);
    }

    public BoostedPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT);
    }

    public BoostedPower(AbstractCreature owner, int amount) {
        super(NAME, TYPE, TURN_BASED, owner, amount);
        loadRegion("doubleDamage");
    }

    @Override
    public void updateDescription() {
        this.description = String.format(DESCRIPTIONS[0], amount);
    }

    @Override
    public void atEndOfTurn(boolean isPlayer) {
        if (!isPlayer) return;

        atb(new DamageRandomEnemyAction(
            new DamageInfo(owner, amount, DamageInfo.DamageType.NORMAL),
            AbstractGameAction.AttackEffect.SLASH_DIAGONAL
        ));
    }
}