package SpireFantasyX.powers;

import com.megacrit.cardcrawl.actions.unique.LoseEnergyAction;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.PoisonPower;

import static SpireFantasyX.util.Wiz.alive_monsters;
import static SpireFantasyX.util.Wiz.apply_enemy;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.get_energy_string;
import static SpireFantasyX.util.Wiz.player;

public class EnergyRainPower extends BasePower {
    public final static String NAME = EnergyRainPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.BUFF;
    public final static boolean TURN_BASED = false;
    public static final int DEFAULT_AMOUNT = 1;

    // Default constructor for AutoAdd
    public EnergyRainPower() {
        this(AbstractDungeon.player);
    }

    public EnergyRainPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT, DEFAULT_AMOUNT);
    }

    public EnergyRainPower(AbstractCreature owner, int poison_amount, int e_loss) {
        super(NAME, TYPE, TURN_BASED, owner, poison_amount, e_loss);
        loadRegion("energized_green");
    }

    @Override
    public void updateDescription() {
        if (amount2 > 0)
             description = String.format(DESCRIPTIONS[0], get_energy_string(amount2, true), amount);
        else description = String.format(DESCRIPTIONS[1], amount);
    }

    @Override
    public void atStartOfTurn() {
        if (owner != player()) return;

        if (amount2 > 0) atb(new LoseEnergyAction(amount2));
        for (AbstractMonster m : alive_monsters()) {
            apply_enemy(m, new PoisonPower(m, owner, amount));
        }
    }
}