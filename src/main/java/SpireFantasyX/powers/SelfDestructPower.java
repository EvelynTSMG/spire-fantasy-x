package SpireFantasyX.powers;

import com.evacipated.cardcrawl.mod.stslib.patches.NeutralPowertypePatch;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction;
import com.megacrit.cardcrawl.actions.common.LoseHPAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

import static SpireFantasyX.util.Wiz.alive_monsters;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.player;

public class SelfDestructPower extends BasePower {
    public final static String NAME = SelfDestructPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.BUFF;
    public final static boolean TURN_BASED = false;
    public static final int DEFAULT_AMOUNT = 5;
    public static final int DEFAULT_AMOUNT2 = 1;
    private final boolean upgraded;

    // Default constructor for AutoAdd
    public SelfDestructPower() {
        this(AbstractDungeon.player);
    }

    public SelfDestructPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT, DEFAULT_AMOUNT2, false);
    }

    public SelfDestructPower(AbstractCreature owner, int dmg, int hp_loss, boolean upgraded) {
        super(NAME, TYPE, TURN_BASED, owner, dmg, hp_loss);
        this.upgraded = upgraded;
        loadRegion("combust");
    }

    @Override
    public void updateDescription() {
        this.description = String.format(DESCRIPTIONS[upgraded ? 1 : 0], amount2, amount);
    }

    @Override
    public void atStartOfTurn() {
        if (owner != player()) return;

        atb(new LoseHPAction(owner, owner, amount2));
        for (int i = upgraded ? alive_monsters().size() : 1; i > 0; i--) {
            atb(new DamageAllEnemiesAction(
                player(), amount,
                DamageInfo.DamageType.NORMAL,
                AbstractGameAction.AttackEffect.FIRE
            ));
        }
    }
}