package SpireFantasyX.powers;

import SpireFantasyX.cards.magic.BaseMagicCard;
import com.evacipated.cardcrawl.mod.stslib.powers.interfaces.NonStackablePower;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

public class ExtractAbilityPower extends BasePower implements NonStackablePower {
    public final static String NAME = ExtractAbilityPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.BUFF;
    public final static boolean TURN_BASED = false;
    public static final int DEFAULT_AMOUNT = 1;

    // Default constructor for AutoAdd
    public ExtractAbilityPower() {
        this(AbstractDungeon.player);
    }

    public ExtractAbilityPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT);
    }

    public ExtractAbilityPower(AbstractCreature owner, int amount) {
        super(NAME, TYPE, TURN_BASED, owner, amount);
        loadRegion("ai");
    }

    @Override
    public void onAfterCardPlayed(AbstractCard c) {
        if (c instanceof BaseMagicCard && c.canUpgrade())
            for (int i = amount; i > 0; i--)
                c.upgrade();
    }
}