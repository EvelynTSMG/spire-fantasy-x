package SpireFantasyX.powers;

import com.megacrit.cardcrawl.actions.common.HealAction;
import com.megacrit.cardcrawl.actions.common.ReducePowerAction;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.localization.PowerStrings;

import static SpireFantasyX.util.Wiz.att;

public class RegenPower extends BasePower {
    public final static String NAME = RegenPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.BUFF;
    public final static boolean TURN_BASED = false;
    public static final int DEFAULT_AMOUNT = 1;

    // Default constructor for AutoAdd
    public RegenPower() {
        this(AbstractDungeon.player);
    }

    public RegenPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT);
    }

    public RegenPower(AbstractCreature owner, int amount) {
        super(NAME, TYPE, TURN_BASED, owner, amount);

        PowerStrings POWER_STRINGS = CardCrawlGame.languagePack.getPowerStrings("Regenerate");
        name = POWER_STRINGS.NAME;
        DESCRIPTIONS = POWER_STRINGS.DESCRIPTIONS;
        updateDescription();
        loadRegion("regen");
    }

    public void updateDescription() {
        this.description = DESCRIPTIONS[0] + amount + DESCRIPTIONS[1];
    }

    public void atEndOfTurn(boolean is_player) {
        flashWithoutSound();
        att(new ReducePowerAction(owner, owner, this, 1));
        att(new HealAction(owner, owner, amount));
    }

    public void stackPower(int stackAmount) {
        super.stackPower(stackAmount);
    }
}