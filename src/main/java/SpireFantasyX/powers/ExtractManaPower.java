package SpireFantasyX.powers;

import SpireFantasyX.powers.interfaces.OnMonsterDeathPower;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.actions.common.GainEnergyAction;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.get_energy_string;

public class ExtractManaPower extends BasePower implements OnMonsterDeathPower {
    public final static String NAME = ExtractManaPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.BUFF;
    public final static boolean TURN_BASED = false;
    public static final int DEFAULT_AMOUNT = 1;

    // Default constructor for AutoAdd
    public ExtractManaPower() {
        this(AbstractDungeon.player);
    }

    public ExtractManaPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT, DEFAULT_AMOUNT);
    }

    public ExtractManaPower(AbstractCreature owner, int card_draw, int e_gain) {
        super(NAME, TYPE, TURN_BASED, owner, card_draw, e_gain);
        loadRegion("reactive");
    }

    @Override
    public void updateDescription() {
        this.description = String.format(DESCRIPTIONS[0],
            amount, amount == 1 ? DESCRIPTIONS[1] : DESCRIPTIONS[2],
            get_energy_string(amount2, true));
    }

    @Override
    public void on_monster_death(AbstractMonster monster) {
        atb(new DrawCardAction(amount));
        atb(new GainEnergyAction(amount2));
    }
}