package SpireFantasyX.powers.interfaces;

import com.megacrit.cardcrawl.monsters.AbstractMonster;

public interface OnMonsterDeathPower {
    void on_monster_death(AbstractMonster monster);
}
