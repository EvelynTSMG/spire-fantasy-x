package SpireFantasyX.powers.interfaces;

import com.megacrit.cardcrawl.cards.DamageInfo;

public interface OnAttackPreBlockToChangeDamagePower {
    int on_attack_pre_block_to_change_damage(DamageInfo info, int damage);
}
