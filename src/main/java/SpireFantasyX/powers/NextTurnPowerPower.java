package SpireFantasyX.powers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.PowerStrings;
import com.megacrit.cardcrawl.powers.AbstractPower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;

/**
 * Shamelessly <s>stolen</s> taken from Mistress Autumn's Starlight Sisters
 */
public class NextTurnPowerPower extends BasePower {
    public static String TEXT_ID = id(NextTurnPowerPower.class.getSimpleName());
    public static PowerStrings strings = CardCrawlGame.languagePack.getPowerStrings(TEXT_ID);
    private AbstractPower power_to_gain;

    public NextTurnPowerPower(AbstractCreature owner, AbstractPower power_to_gain) {
        super(TEXT_ID + power_to_gain.ID, power_to_gain.type, false, owner, power_to_gain.amount);
        ID = TEXT_ID;
        this.name = String.format(strings.NAME, power_to_gain.name);
        DESCRIPTIONS = strings.DESCRIPTIONS;
        this.img = power_to_gain.img;
        this.region48 = power_to_gain.region48;
        this.region128 = power_to_gain.region128;
        this.power_to_gain = power_to_gain;
        updateDescription();
    }

    @Override
    public void renderIcons(SpriteBatch sb, float x, float y, Color c) {
        super.renderIcons(sb, x, y, Color.GREEN.cpy());
    }

    @Override
    public void stackPower(int stackAmount) {
        super.stackPower(stackAmount);
        power_to_gain.amount += stackAmount;
    }

    @Override
    public void atEndOfTurn(boolean isPlayer) {
        flash();
        atb(new ApplyPowerAction(owner, owner, power_to_gain, power_to_gain.amount));
        atb(new RemoveSpecificPowerAction(owner, owner, ID));
    }

    @Override
    public void updateDescription() {
        if (power_to_gain == null) {
            description = "???";
        } else {
            description = String.format(DESCRIPTIONS[0], power_to_gain.amount, power_to_gain.name);
        }
    }
}