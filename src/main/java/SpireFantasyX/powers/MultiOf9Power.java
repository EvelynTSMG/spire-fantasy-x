package SpireFantasyX.powers;

import SpireFantasyX.powers.interfaces.OnAttackPreBlockToChangeDamagePower;
import com.evacipated.cardcrawl.mod.stslib.powers.interfaces.NonStackablePower;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class MultiOf9Power extends BasePower implements NonStackablePower, OnAttackPreBlockToChangeDamagePower {
    public final static String NAME = MultiOf9Power.class.getSimpleName();
    public final static PowerType TYPE = PowerType.BUFF;
    public final static boolean TURN_BASED = false;
    public static final int DEFAULT_AMOUNT = 1;

    // Default constructor for AutoAdd
    public MultiOf9Power() {
        this(AbstractDungeon.player);
    }

    public MultiOf9Power(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT, DEFAULT_AMOUNT);
    }

    public MultiOf9Power(AbstractCreature owner, int hits, int dmg) {
        super(NAME, TYPE, TURN_BASED, owner, hits, dmg);
        loadRegion("closeUp");
    }

    @Override
    public void updateDescription() {
        if (amount == 0) name = DESCRIPTIONS[5];
        else if (amount <= 10) name = DESCRIPTIONS[amount];
        else name = new String(new char[amount]).replace('\0', '9');
        description = String.format(DESCRIPTIONS[0], amount, amount2);
    }



    @Override
    public int on_attack_pre_block_to_change_damage(DamageInfo info, int damage) {
        if (info.type == DamageInfo.DamageType.NORMAL && damage < amount2) {
            damage = amount2;
            amount--;
        }

        if (amount <= 0) remove_this();

        return damage;
    }

    @Override
    public boolean isStackable(AbstractPower power) {
        return power instanceof MultiOf9Power && ((MultiOf9Power)power).amount2 == amount2;
    }
}