package SpireFantasyX.powers;

import com.megacrit.cardcrawl.actions.common.GainEnergyAction;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.get_energy_string;
import static SpireFantasyX.util.Wiz.player;

public class MpStrollPower extends BasePower {
    public final static String NAME = MpStrollPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.BUFF;
    public final static boolean TURN_BASED = false;
    public static final int DEFAULT_AMOUNT = 1;

    // Default constructor for AutoAdd
    public MpStrollPower() {
        this(AbstractDungeon.player);
    }

    public MpStrollPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT, DEFAULT_AMOUNT);
    }

    public MpStrollPower(AbstractCreature owner, int turns, int e_gain) {
        super(NAME, TYPE, TURN_BASED, owner, turns, e_gain);
        loadRegion("energized_blue");
    }

    @Override
    public void updateDescription() {
        this.description = String.format(DESCRIPTIONS[0], amount, get_energy_string(amount2, true));
    }

    @Override
    public void atStartOfTurn() {
        if (owner != player()) return;

        flash();
        atb(new GainEnergyAction(amount2));
        amount--;
        if (amount <= 0) remove_this();
    }
}