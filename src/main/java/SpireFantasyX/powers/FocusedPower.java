package SpireFantasyX.powers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.evacipated.cardcrawl.mod.stslib.powers.interfaces.NonStackablePower;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;

import static SpireFantasyX.util.Wiz.atb;

public class FocusedPower extends BasePower implements NonStackablePower {
    public final static String NAME = FocusedPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.BUFF;
    public final static boolean TURN_BASED = false;
    public static final int DEFAULT_AMOUNT = 1;

    // Default constructor for AutoAdd
    public FocusedPower() {
        this(AbstractDungeon.player);
    }

    public FocusedPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT);
    }

    public FocusedPower(AbstractCreature owner, int amount) {
        super(NAME, TYPE, TURN_BASED, owner, amount);
        loadRegion("focus");
    }

    @Override
    public void atEndOfTurn(boolean isPlayer) {
        if (!isPlayer) return;

        if (owner.currentBlock > 1)  atb(new GainBlockAction(owner, owner.currentBlock / 2));
    }

    @Override
    public void renderAmount(SpriteBatch sb, float x, float y, Color c) { }
}