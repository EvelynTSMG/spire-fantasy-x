package SpireFantasyX.cards;

import SpireFantasyX.powers.EnergyRainPower;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_self;

public class EnergyRain extends BaseCard {
    public final static String NAME = EnergyRain.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.POWER;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = 0;
    public final static int UP_COST = 2;
    public final static int MAGIC = 3;
    public final static int UP_MAGIC = 2;
    public final static int MAGIC2 = 1;
    public final static int UP_MAGIC2 = -1;

    public EnergyRain() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
        magic2(MAGIC2);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_self(new EnergyRainPower(p, magicNumber, magic2));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        if (timesUpgraded == 1) upgradeMagic2(UP_MAGIC2);
        upgradeBaseCost(UP_COST);
        updateDescription();
    }
}