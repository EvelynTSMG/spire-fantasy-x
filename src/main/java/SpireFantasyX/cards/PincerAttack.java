package SpireFantasyX.cards;

import SpireFantasyX.actions.BranchingAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.SurroundedPower;
import com.megacrit.cardcrawl.powers.VulnerablePower;
import com.megacrit.cardcrawl.powers.WeakPower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_enemy;
import static SpireFantasyX.util.Wiz.atb;

public class PincerAttack extends BaseCard {
    public final static String NAME = PincerAttack.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int BLOCK = 8;
    public final static int UP_BLOCK = 6;
    public final static int MAGIC = 2;
    public final static int UP_MAGIC = 1;
    public final static int MAGIC2 = 1;

    public PincerAttack() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        block(BLOCK);
        magic(MAGIC);
        magic2(MAGIC2);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_enemy(m, new VulnerablePower(m, magicNumber, false));
        apply_enemy(m, new WeakPower(m, magic2, false));
        atb(new BranchingAction(
            () -> p.hasPower(SurroundedPower.POWER_ID),
            new GainBlockAction(p, block)
        ));
    }

    @Override
    public void upp() {
        upgradeBlock(UP_BLOCK);
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}