package SpireFantasyX.cards;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.GainStrengthPower;
import com.megacrit.cardcrawl.powers.StrengthPower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_enemy;

public class PowerBreak extends BaseCard {
    public final static String NAME = PowerBreak.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int COST = 1;
    public final static int DMG = 4;
    public final static int UP_DMG = 2;
    public final static int MAGIC = 2;
    public final static int UP_MAGIC = 1;

    public PowerBreak() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        damage(m, AbstractGameAction.AttackEffect.SLASH_HORIZONTAL);
        apply_enemy(m, new StrengthPower(m, -magicNumber));
        apply_enemy(m, new GainStrengthPower(m, magicNumber));
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}