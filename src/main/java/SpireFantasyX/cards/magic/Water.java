package SpireFantasyX.cards.magic;

import SpireFantasyX.actions.InstantAction;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;

public class Water extends BaseMagicCard {
    public final static String NAME = Water.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int COST = 1;
    public final static int UP_COST = 2;
    public final static int MAGIC = 2;
    public final static int UP_MAGIC = 1;
    public final static int UP_MAGIC2 = 2;

    public Water() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new DrawCardAction(magicNumber, new InstantAction(() -> {
            for (AbstractCard c : DrawCardAction.drawnCards) {
                c.upgrade();
            }
        })));
    }

    @Override
    public void upgradeName() {
        ++this.timesUpgraded;
        this.upgraded = true;
        this.name = CARD_STRINGS.EXTENDED_DESCRIPTION[timesUpgraded > 1 ? 1 : 0];
        this.initializeTitle();
    }

    @Override
    public void upp() {
        upgradeMagicNumber(timesUpgraded == 2 ? UP_MAGIC2 : UP_MAGIC);
        if (timesUpgraded == 2) upgradeBaseCost(UP_COST);
        updateDescription();
    }
}