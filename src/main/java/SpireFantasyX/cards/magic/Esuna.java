package SpireFantasyX.cards.magic;

import SpireFantasyX.actions.BranchingAction;
import SpireFantasyX.actions.CleansePowerAction;
import SpireFantasyX.actions.InstantAction;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.AbstractPower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.att;

public class Esuna extends BaseMagicCard {
    public final static String NAME = Esuna.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int BLOCK = 4;
    public final static int UP_BLOCK = 3;
    public final static int MAGIC = 1;
    public final static int UP_MAGIC = 1;

    public Esuna() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        block(BLOCK);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        block();
        atb(new BranchingAction(
            () -> magicNumber > 2,
            new InstantAction(() -> {
                for (AbstractPower pow : p.powers) {
                    if (pow.type != AbstractPower.PowerType.DEBUFF) continue;
                    att(new RemoveSpecificPowerAction(p, p, pow));
                }
            }),
            new CleansePowerAction(p, magicNumber, pow -> pow.type == AbstractPower.PowerType.DEBUFF)
        ));
        super.use(p, m);
    }

    @Override
    public void updateDescription() {
        if (baseMagicNumber > 2) rawDescription = CARD_STRINGS.UPGRADE_DESCRIPTION;
        initializeDescription();
    }

    @Override
    public void upp() {
        baseBlock += timesUpgraded < 2 ? UP_BLOCK : (UP_BLOCK + timesUpgraded);
        baseMagicNumber += timesUpgraded;

        upgradedBlock = true;
        upgradedMagicNumber = true;
        updateDescription();
    }
}