package SpireFantasyX.cards.magic;

import SpireFantasyX.actions.CleansePowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.AbstractPower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;

public class Dispel extends BaseMagicCard {
    public final static String NAME = Dispel.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 2;
    public final static int UP_COST = 1;
    public final static int MAGIC = 1;
    public final static int UP_MAGIC = 1;

    public Dispel() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new CleansePowerAction(m, magicNumber, pow -> pow.type == AbstractPower.PowerType.BUFF));
        super.use(p, m);
    }

    @Override
    public void upp() {
        if (timesUpgraded > 1) upgradeMagicNumber(UP_MAGIC);
        upgradeBaseCost(UP_COST);
        updateDescription();
    }
}