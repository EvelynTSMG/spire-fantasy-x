package SpireFantasyX.cards.magic;

import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.DexterityPower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.alive_monsters;
import static SpireFantasyX.util.Wiz.apply_self;

public class Haste extends BaseMagicCard {
    public final static String NAME = Haste.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.POWER;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int MAGIC = 2;
    public final static int UP_MAGIC = 1;

    public Haste() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        if (upgraded) {
            for (int i = alive_monsters().size(); i > 0; i--)
                apply_self(new DexterityPower(p, magicNumber));
        } else {
            apply_self(new DexterityPower(p, magicNumber));
        }
        super.use(p, m);
    }

    @Override
    public void upgradeName() {
        ++this.timesUpgraded;
        this.upgraded = true;
        this.name = CARD_STRINGS.EXTENDED_DESCRIPTION[0];
        if (timesUpgraded > 1) name += timesUpgraded - 2 == 0 ? "+" : String.format("+%d", timesUpgraded - 1);
        this.initializeTitle();
    }

    @Override
    public void upp() {
        if (timesUpgraded > 1) upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}