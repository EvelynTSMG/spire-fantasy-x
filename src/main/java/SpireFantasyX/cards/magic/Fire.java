package SpireFantasyX.cards.magic;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;

public class Fire extends BaseMagicCard {
    public final static String NAME = Fire.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int COST = 1;
    public final static int UP_COST = 2;
    public final static int DMG = 6;
    public final static int UP_DMG = 6;
    public final static int UP_DMG2 = 12;

    public Fire() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        damage(m, AbstractGameAction.AttackEffect.FIRE);
    }

    @Override
    public void upgradeName() {
        ++this.timesUpgraded;
        this.upgraded = true;
        this.name = CARD_STRINGS.EXTENDED_DESCRIPTION[timesUpgraded > 1 ? 1 : 0];
        this.initializeTitle();
    }

    @Override
    public void upp() {
        upgradeDamage(timesUpgraded == 2 ? UP_DMG2 : UP_DMG);
        if (timesUpgraded == 1) upgradeBaseCost(UP_COST);
        updateDescription();
    }
}