package SpireFantasyX.cards.magic;

import SpireFantasyX.actions.BranchingAction;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;

public class Drain extends BaseMagicCard {
    public final static String NAME = Drain.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = 2;
    public final static int UP_COST = 3;
    public final static int DMG = 15;
    public final static int UP_DMG = 10;
    public final static int UP_DMG2 = 15;
    public final static int DMG2 = 5;

    public Drain() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        dmg2(DMG2);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        damage(m, AbstractGameAction.AttackEffect.SLASH_HEAVY);
        atb(new BranchingAction(
            () -> upgraded && m.currentHealth < m.maxHealth,
            new DamageAction(m,
                new DamageInfo(p, dmg2, damageTypeForTurn),
                AbstractGameAction.AttackEffect.SLASH_DIAGONAL
            )
        ));
    }

    @Override
    public void upgradeName() {
        ++this.timesUpgraded;
        this.upgraded = true;
        this.name = CARD_STRINGS.EXTENDED_DESCRIPTION[timesUpgraded > 1 ? 1 : 0];
        this.initializeTitle();
    }

    @Override
    public void upp() {
        upgradeDamage(timesUpgraded == 2 ? UP_DMG2 : UP_DMG);
        if (timesUpgraded > 1) upgradeBaseCost(UP_COST);
        updateDescription();
    }
}