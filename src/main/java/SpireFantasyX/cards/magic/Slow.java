package SpireFantasyX.cards.magic;

import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.SlowPower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.alive_monsters;
import static SpireFantasyX.util.Wiz.apply_enemy;

public class Slow extends BaseMagicCard {
    public final static String NAME = Slow.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 2;
    public final static int UP_COST = 1;

    public Slow() {
        super(NAME, COST, TYPE, RARITY, TARGET);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        if (upgraded) {
            for (AbstractMonster mon : alive_monsters())
                apply_enemy(mon, new SlowPower(mon, 1));
        } else {
            apply_enemy(m, new SlowPower(m, 1));
        }
        super.use(p, m);
    }

    @Override
    public void upgradeName() {
        ++this.timesUpgraded;
        this.upgraded = true;
        this.name = CARD_STRINGS.EXTENDED_DESCRIPTION[0];
        if (timesUpgraded > 1) name += timesUpgraded - 2 == 0 ? "+" : String.format("+%d", timesUpgraded - 1);
        this.initializeTitle();
    }

    @Override
    public void upp() {
        if (timesUpgraded > 1) upgradeBaseCost(UP_COST);
        exhaust = true;
        target = CardTarget.ALL_ENEMY;
        updateDescription();
    }
}