package SpireFantasyX.cards.magic;

import SpireFantasyX.actions.XCostAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.SpireFantasyXMod.magic_cards_played;
import static SpireFantasyX.util.Wiz.atb;

public class Holy extends BaseMagicCard {
    public final static String NAME = Holy.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = -1;
    public final static int MAGIC = 0;
    public final static int UP_MAGIC = 1;

    public Holy() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new XCostAction(this, (x, ignored) -> {
            for (int i = magic_cards_played; i > 0; i--)
                atb(new DamageAction(m, new DamageInfo(p, cost)));
            return true;
        }));

        super.use(p, m);
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}