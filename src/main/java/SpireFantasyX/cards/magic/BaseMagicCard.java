package SpireFantasyX.cards.magic;

import SpireFantasyX.SpireFantasyXMod;
import SpireFantasyX.actions.InstantAction;
import SpireFantasyX.cards.BaseCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.util.Wiz.atb;

public abstract class BaseMagicCard extends BaseCard {
    public BaseMagicCard(String name, int cost, CardType type, CardRarity rarity, CardTarget target) {
        super(name, cost, type, rarity, target);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new InstantAction(() -> SpireFantasyXMod.magic_cards_played += 1));
    }

    @Override
    public boolean canUpgrade() {
        return timesUpgraded < 2;
    }
}
