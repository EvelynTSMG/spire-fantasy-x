package SpireFantasyX.cards.magic;

import SpireFantasyX.actions.InstantAction;
import com.evacipated.cardcrawl.mod.stslib.fields.cards.AbstractCard.FleetingField;
import com.megacrit.cardcrawl.actions.unique.AddCardToDeckAction;
import com.megacrit.cardcrawl.cards.colorless.BandageUp;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.relics.LizardTail;
import com.megacrit.cardcrawl.relics.Strawberry;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.room;

public class Life extends BaseMagicCard {
    public final static String NAME = Life.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = 2;

    public Life() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        FleetingField.fleeting.set(this, true);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        if (timesUpgraded == 0) atb(new AddCardToDeckAction(new BandageUp()));
        else if (timesUpgraded == 1)
            atb(new InstantAction(
                () -> room().spawnRelicAndObtain(Settings.WIDTH/2f, Settings.HEIGHT/2f, new Strawberry())
            ));
        else if (timesUpgraded >= 2)
            atb(new InstantAction(
                () -> room().spawnRelicAndObtain(Settings.WIDTH/2f, Settings.HEIGHT/2f, new LizardTail())
            ));
        super.use(p, m);
    }

    @Override
    public void upgradeName() {
        ++this.timesUpgraded;
        this.upgraded = true;
        this.name = CARD_STRINGS.EXTENDED_DESCRIPTION[timesUpgraded > 1 ? 2 : 0];
        this.initializeTitle();
    }

    @Override
    public void updateDescription() {
        if (timesUpgraded > 0) rawDescription = CARD_STRINGS.EXTENDED_DESCRIPTION[timesUpgraded > 1 ? 3 : 1];
        initializeDescription();
    }

    @Override
    public void upp() {
        updateDescription();
    }
}