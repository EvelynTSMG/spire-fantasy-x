package SpireFantasyX.cards.magic;

import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.ThornsPower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_self;

public class Shell extends BaseMagicCard {
    public final static String NAME = Shell.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int UP_COST = 2;
    public final static int BLOCK = 9;
    public final static int UP_BLOCK = 4;
    public final static int MAGIC = 3;

    public Shell() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        block(BLOCK);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        block();
        if (timesUpgraded > 1) apply_self(new ThornsPower(p, magicNumber));
        super.use(p, m);
    }

    @Override
    public void upgradeName() {
        ++this.timesUpgraded;
        this.upgraded = true;
        this.name = CARD_STRINGS.EXTENDED_DESCRIPTION[timesUpgraded > 1 ? 1 : 0];
        this.initializeTitle();
    }

    @Override
    public void updateDescription() {
        if (timesUpgraded > 1) rawDescription = CARD_STRINGS.UPGRADE_DESCRIPTION;
        initializeDescription();
    }

    @Override
    public void upp() {
        if (timesUpgraded > 1) upgradeBaseCost(UP_COST);
        upgradeBlock(UP_BLOCK);
        updateDescription();
    }
}