package SpireFantasyX.cards.magic;

import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;

public class Blizzard extends BaseMagicCard {
    public final static String NAME = Blizzard.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int COST = 1;
    public final static int UP_COST = 2;
    public final static int BLOCK = 8;
    public final static int UP_BLOCK = 8;

    public Blizzard() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        block(BLOCK);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        block();
    }

    @Override
    public void upgradeName() {
        ++this.timesUpgraded;
        this.upgraded = true;
        this.name = CARD_STRINGS.EXTENDED_DESCRIPTION[timesUpgraded > 1 ? 1 : 0];
        this.initializeTitle();
    }

    @Override
    public void upp() {
        upgradeBlock(UP_BLOCK);
        if (timesUpgraded == 1) upgradeBaseCost(UP_COST);
        updateDescription();
    }
}