package SpireFantasyX.cards.magic;

import SpireFantasyX.powers.JinxPower;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_enemy;

public class Thunder extends BaseMagicCard {
    public final static String NAME = Thunder.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.BASIC;

    public final static int COST = 1;
    public final static int UP_COST = 2;
    public final static int DMG = 4;
    public final static int UP_DMG = 4;
    public final static int UP_DMG2 = 6;
    public final static int MAGIC = 5;
    public final static int UP_MAGIC = 2;
    public final static int UP_MAGIC2 = 3;

    public Thunder() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        damage(m, AbstractGameAction.AttackEffect.LIGHTNING);
        apply_enemy(m, new JinxPower(m, magicNumber));
    }

    @Override
    public void upgradeName() {
        ++this.timesUpgraded;
        this.upgraded = true;
        this.name = CARD_STRINGS.EXTENDED_DESCRIPTION[timesUpgraded > 1 ? 1 : 0];
        this.initializeTitle();
    }

    @Override
    public void upp() {
        upgradeDamage(timesUpgraded == 2 ? UP_DMG2 : UP_DMG);
        upgradeMagicNumber(timesUpgraded == 2 ? UP_MAGIC2 : UP_MAGIC);
        if (timesUpgraded == 1) upgradeBaseCost(UP_COST);
        updateDescription();
    }
}