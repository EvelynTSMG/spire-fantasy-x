package SpireFantasyX.cards;

import SpireFantasyX.powers.DoomPower;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_self;
import static java.lang.Math.min;

public class Doom extends BaseCard {
    public final static String NAME = Doom.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.NONE;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 2;
    public final static int MAGIC = 3;
    public final static int UP_MAGIC = -1;

    public Doom() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_self(new DoomPower(p, magicNumber));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(min(UP_MAGIC, magicNumber));
        updateDescription();
    }
}