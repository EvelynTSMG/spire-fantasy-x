package SpireFantasyX.cards;

import SpireFantasyX.powers.RegenPower;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.alive_monsters;
import static SpireFantasyX.util.Wiz.apply_enemy;
import static SpireFantasyX.util.Wiz.apply_self;

public class Pray extends BaseCard {
    public final static String NAME = Pray.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.ALL;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 2;
    public final static int MAGIC = 4;
    public final static int UP_MAGIC = 2;

    public Pray() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
        exhaust = true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster _m) {
        apply_self(new RegenPower(p, magicNumber));
        for (AbstractMonster m : alive_monsters()) {
            apply_enemy(m, new RegenPower(m, magicNumber));
        }
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}