package SpireFantasyX.cards;

import com.megacrit.cardcrawl.actions.unique.GreedAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;

public class Pickpocket extends BaseCard {
    public final static String NAME = Pickpocket.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int DMG = 9;
    public final static int UP_DMG = 4;
    public final static int MAGIC = 10;
    public final static int UP_MAGIC = 5;

    public Pickpocket() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new GreedAction(m, new DamageInfo(p, damage, damageTypeForTurn), magicNumber));
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}