package SpireFantasyX.cards;

import SpireFantasyX.actions.BetterSelectCardsInHandAction;
import SpireFantasyX.actions.InstantAction;
import basemod.ReflectionHacks;
import com.megacrit.cardcrawl.actions.common.ExhaustAction;
import com.megacrit.cardcrawl.actions.common.ExhaustSpecificCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.localization.UIStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.monsters.EnemyMoveInfo;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_self;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.att;

public class WhiteWind extends BaseCard {
    private final static String[] TEXT = CardCrawlGame.languagePack.getUIString(id("ChoiceExhaust")).TEXT;

    public final static String NAME = WhiteWind.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int BLOCK = 12;
    public final static int UP_BLOCK = 4;
    public final static int MAGIC = 1;

    public WhiteWind() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        block(BLOCK);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        block();
        atb(new BetterSelectCardsInHandAction(
            magicNumber,
            String.format(TEXT[0], magicNumber == 1 ? TEXT[1] : String.format(TEXT[2], magicNumber)),
            false, false,
            c -> c.type == CardType.STATUS || c.type == CardType.CURSE,
            l -> {
                for (AbstractCard c : l) {
                    att(new ExhaustSpecificCardAction(c, p.hand));
                }
            }
        ));
    }

    @Override
    public void upp() {
        upgradeBlock(UP_BLOCK);
        updateDescription();
    }
}