package SpireFantasyX.cards;

import SpireFantasyX.actions.InstantAction;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.animations.VFXAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.vfx.combat.BlizzardEffect;
import com.megacrit.cardcrawl.vfx.combat.ExplosionSmallEffect;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;

public class DeltaAttack extends BaseCard {
    public final static String NAME = DeltaAttack.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = 3;
    public final static int DMG = 15;
    public final static int UP_DMG = 5;
    public final static int MAGIC = 15;

    public DeltaAttack() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new VFXAction(new BlizzardEffect(1, false), 0.1f));
        atb(new VFXAction(new ExplosionSmallEffect(m.hb.cX, m.hb.cY), 0.1f));
        damage(m, AbstractGameAction.AttackEffect.LIGHTNING);
        atb(new InstantAction(() -> {
            baseDamage += magicNumber;
            applyPowers();
        }));
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
        updateDescription();
    }
}