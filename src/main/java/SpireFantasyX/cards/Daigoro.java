package SpireFantasyX.cards;

import SpireFantasyX.actions.BranchingAction;
import com.megacrit.cardcrawl.actions.common.DiscardAction;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.actions.common.ExhaustAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;

public class Daigoro extends BaseCard {
    public final static String NAME = Daigoro.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.NONE;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 0;
    public final static int MAGIC = 1;
    public final static int UP_MAGIC = 1;
    public final static int MAGIC2 = 1;

    public Daigoro() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
        magic2(MAGIC2);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new DrawCardAction(magicNumber));
        atb(new BranchingAction(
            () -> upgraded,
            new ExhaustAction(magic2, false),
            new DiscardAction(p, p, magic2, false, false)
        ));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}