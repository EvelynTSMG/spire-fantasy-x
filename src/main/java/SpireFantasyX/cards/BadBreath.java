package SpireFantasyX.cards;

import SpireFantasyX.powers.BadBreathPower;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_self;

public class BadBreath extends BaseCard {
    public final static String NAME = BadBreath.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.POWER;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 2;
    public final static int MAGIC = 3;
    public final static int UP_MAGIC = 1;

    public BadBreath() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_self(new BadBreathPower(p, m, magicNumber));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}