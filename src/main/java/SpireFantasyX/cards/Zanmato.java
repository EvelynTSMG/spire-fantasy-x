package SpireFantasyX.cards;

import SpireFantasyX.SpireFantasyXMod;
import SpireFantasyX.actions.InstantAction;
import com.megacrit.cardcrawl.actions.common.InstantKillAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.alive_monsters;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.att;
import static java.lang.Math.log10;
import static java.lang.Math.max;
import static java.lang.Math.sqrt;

public class Zanmato extends BaseCard {
    public final static String NAME = Zanmato.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ALL_ENEMY;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = 2;
    public final static int MAGIC = 5;
    public final static int UP_MAGIC = 5;

    public Zanmato() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
        initializeDescription();
        exhaust = true;
    }

    public int get_kill_threshold() {
        return max(magicNumber, magicNumber + (int)(sqrt(CardCrawlGame.goldGained * 3.6f)));
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new InstantAction(() -> {
            int threshold = get_kill_threshold();
            for (AbstractMonster mon : alive_monsters()) {
                if (mon.currentHealth < threshold) {
                    att(new InstantKillAction(mon));
                }
            }
        }));
    }

    @Override
    public void initializeDescription() {
        magic2 = get_kill_threshold();
        is_magic2_modified = magic2 != magicNumber;
        super.initializeDescription();
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}