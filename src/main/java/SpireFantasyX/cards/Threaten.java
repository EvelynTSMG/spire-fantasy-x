package SpireFantasyX.cards;

import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.GainStrengthPower;
import com.megacrit.cardcrawl.powers.StrengthPower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_enemy;

public class Threaten extends BaseCard {
    public final static String NAME = Threaten.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int MAGIC = 3;
    public final static int UP_MAGIC = 4;

    public Threaten() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
        isEthereal = true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_enemy(m, new StrengthPower(m, -magicNumber));
        apply_enemy(m, new GainStrengthPower(m, magicNumber));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}