package SpireFantasyX.cards;

import SpireFantasyX.powers.JinxPower;
import SpireFantasyX.powers.NextTurnPowerPower;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_enemy;

public class Aim extends BaseCard {
    public final static String NAME = Aim.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int COST = 1;
    public final static int MAGIC = 3;
    public final static int UP_MAGIC = 2;

    public Aim() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_enemy(m, new JinxPower(m, magicNumber));
        apply_enemy(m, new NextTurnPowerPower(m, new JinxPower(m, magicNumber)));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}