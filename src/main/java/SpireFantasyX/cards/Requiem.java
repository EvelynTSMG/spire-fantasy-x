package SpireFantasyX.cards;

import SpireFantasyX.SpireFantasyXMod;
import SpireFantasyX.actions.InstantAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.att;

public class Requiem extends BaseCard {
    public final static String NAME = Requiem.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = 1;
    public final static int BLOCK = 10;
    public final static int UP_BLOCK = 10;

    public Requiem() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        block(BLOCK);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new InstantAction(() -> {
            for (int i = SpireFantasyXMod.enemies_killed; i > 0; i--)
                att(new GainBlockAction(p, magicNumber));
        }));
    }

    @Override
    public void upp() {
        upgradeBlock(UP_BLOCK);
        updateDescription();
    }
}