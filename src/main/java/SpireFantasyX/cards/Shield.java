package SpireFantasyX.cards;

import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.MetallicizePower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_self;

public class Shield extends BaseCard {
    public final static String NAME = Shield.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.POWER;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int MAGIC = 2;
    public final static int UP_MAGIC = 1;

    public Shield() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_self(new MetallicizePower(p, magicNumber));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}