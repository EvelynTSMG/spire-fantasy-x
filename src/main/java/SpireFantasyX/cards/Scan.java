package SpireFantasyX.cards;

import basemod.ReflectionHacks;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.alive_monsters;
import static SpireFantasyX.util.Wiz.is_in_combat;
import static SpireFantasyX.util.Wiz.player;

public class Scan extends BaseCard {
    public final static String NAME = Scan.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.CURSE;
    public final static CardTarget TARGET = CardTarget.NONE;
    public final static CardRarity RARITY = CardRarity.SPECIAL;

    public final static int COST = -2;

    public Scan() {
        super(NAME, COST, TYPE, RARITY, TARGET);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) { }

    @Override
    public void upp() { }

    @Override
    public void render(SpriteBatch sb) {
        super.render(sb);

        if (!is_in_combat()) return;

        sb.setColor(Color.WHITE);

        for (AbstractMonster m : alive_monsters()) {
            String anim = ((AbstractCreature.CreatureAnimation)
                ReflectionHacks.getPrivateInherited(m, AbstractCreature.class, "animation")).name();

            float anim_timer =
                ReflectionHacks.getPrivateInherited(m, AbstractCreature.class, "animationTimer");

            FontHelper.renderFontCentered(
                sb,
                FontHelper.cardDescFont_N,
                String.format("%s: %.2f", anim, anim_timer),
                m.hb.cX, m.hb.cY + (m.hb.cY -  m.healthHb.cY)
            );
        }

        int[] rng_counters = {
            AbstractDungeon.monsterRng.counter, AbstractDungeon.mapRng.counter,
            AbstractDungeon.eventRng.counter, AbstractDungeon.merchantRng.counter,
            AbstractDungeon.cardRng.counter, AbstractDungeon.treasureRng.counter,
            AbstractDungeon.relicRng.counter, AbstractDungeon.potionRng.counter,
            AbstractDungeon.monsterHpRng.counter, AbstractDungeon.aiRng.counter,
            AbstractDungeon.shuffleRng.counter, AbstractDungeon.cardRandomRng.counter,
            AbstractDungeon.miscRng.counter
        };

        float avg_mx = alive_monsters().stream().map((m) -> m.hb.cX).reduce(Float::sum).orElse(Settings.WIDTH/2f);
        float avg_x = (player().hb.cX + avg_mx) / 2f;

        for (int i = 0; i < rng_counters.length; i++) {
            FontHelper.renderFontCentered(
                sb,
                FontHelper.cardDescFont_N,
                String.format("%d", rng_counters[i]),
                avg_x, (0.2f + 0.05f * i) * Settings.HEIGHT
            );
        }
    }
}