package SpireFantasyX.cards;

import com.evacipated.cardcrawl.mod.stslib.actions.common.DamageCallbackAction;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.HealAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.att;

public class Lancet extends BaseCard {
    public final static String NAME = Lancet.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int DMG = 10;
    public final static int UP_DMG = 4;
    public final static int MAGIC = 10;

    public Lancet() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new DamageCallbackAction(
            m, new DamageInfo(p, damage, damageTypeForTurn),
            AbstractGameAction.AttackEffect.BLUNT_LIGHT,
            (unblocked_damage) -> {
                if (unblocked_damage * MAGIC/100 > 0) {
                    att(new HealAction(p, p, unblocked_damage * MAGIC/100));
                }
            }
        ));
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
        updateDescription();
    }
}