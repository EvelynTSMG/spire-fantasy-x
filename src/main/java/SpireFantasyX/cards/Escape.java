package SpireFantasyX.cards;

import SpireFantasyX.actions.BranchingAction;
import SpireFantasyX.actions.InstantAction;
import com.megacrit.cardcrawl.actions.animations.VFXAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.BackAttackPower;
import com.megacrit.cardcrawl.rooms.AbstractRoom;
import com.megacrit.cardcrawl.vfx.combat.SmokeBombEffect;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.player;
import static SpireFantasyX.util.Wiz.room;

public class Escape extends BaseCard {
    public final static String NAME = Escape.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int MAGIC = 10;
    public final static int UP_MAGIC = 10;

    public Escape() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
        exhaust = true;
    }

    @Override
    public boolean canUse(AbstractPlayer p, AbstractMonster m) {
        return super.canUse(p, m)
            && AbstractDungeon.getMonsters().monsters
                .stream()
                .noneMatch(mon -> mon.type == AbstractMonster.EnemyType.BOSS
                        || mon.hasPower(BackAttackPower.POWER_ID));
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new BranchingAction(
            () -> AbstractDungeon.cardRng.randomBoolean((float)magicNumber/100f),
            new InstantAction(() -> {
                if (room().phase == AbstractRoom.RoomPhase.COMBAT) {
                    room().smoked = true;
                    atb(new VFXAction(new SmokeBombEffect(p.hb.cX, p.hb.cY)));
                    player().hideHealthBar();
                    player().isEscaping = true;
                    player().flipHorizontal = !player().flipHorizontal;
                    AbstractDungeon.overlayMenu.endTurnButton.disable();
                    player().escapeTimer = 2.5F;
                }
            })
        ));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}