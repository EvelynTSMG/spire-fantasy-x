package SpireFantasyX.cards;

import SpireFantasyX.damagemods.PiercingDamage;
import com.evacipated.cardcrawl.mod.stslib.damagemods.DamageModifierManager;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.VulnerablePower;
import com.megacrit.cardcrawl.powers.WeakPower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_enemy;

public class FullBreak extends BaseCard {
    public final static String NAME = FullBreak.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = 3;
    public final static int DMG = 20;
    public final static int UP_DMG = 10;
    public final static int MAGIC = 1;
    public final static int UP_MAGIC = 1;

    public FullBreak() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        magic(MAGIC);
        DamageModifierManager.addModifier(this, new PiercingDamage());
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        damage(m, AbstractGameAction.AttackEffect.SLASH_HEAVY);
        apply_enemy(m, new WeakPower(m, magicNumber, false));
        apply_enemy(m, new VulnerablePower(m, magicNumber, false));
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}