package SpireFantasyX.cards;

import SpireFantasyX.powers.ExtractManaPower;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_self;

public class ExtractMana extends BaseCard {
    public final static String NAME = ExtractMana.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.POWER;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = 2;
    public final static int MAGIC = 1;
    public final static int UP_MAGIC = 1;
    public final static int MAGIC2 = 1;

    public ExtractMana() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
        magic2(MAGIC2);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_self(new ExtractManaPower(p, magicNumber, magic2));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}