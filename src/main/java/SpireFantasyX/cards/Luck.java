package SpireFantasyX.cards;

import SpireFantasyX.actions.InstantAction;
import SpireFantasyX.powers.JinxPower;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.VulnerablePower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_enemy_now;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.att;

public class Luck extends BaseCard {
    public final static String NAME = Luck.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = 2;

    public Luck() {
        super(NAME, COST, TYPE, RARITY, TARGET);
    }

    @Override
    public boolean canUse(AbstractPlayer p, AbstractMonster m) {
        return super.canUse(p, m) && m.hasPower(JinxPower.POWER_ID);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new InstantAction(() -> {
            int amount = m.getPower(JinxPower.POWER_ID).amount;

            // att so we have to apply in reverse!
            apply_enemy_now(m, new VulnerablePower(m, amount, false));
            att(new RemoveSpecificPowerAction(m, p, JinxPower.POWER_ID));
        }));
    }

    @Override
    public void upp() {
        retain = true;
        updateDescription();
    }
}