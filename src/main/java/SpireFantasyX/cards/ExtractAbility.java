package SpireFantasyX.cards;

import SpireFantasyX.powers.ExtractAbilityPower;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_self;

public class ExtractAbility extends BaseCard {
    public final static String NAME = ExtractAbility.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.POWER;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = 1;
    public final static int UP_COST = 0;
    public final static int MAGIC = 1;

    public ExtractAbility() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_self(new ExtractAbilityPower(p, magicNumber));
    }

    @Override
    public void upp() {
        upgradeBaseCost(UP_COST);
        updateDescription();
    }
}