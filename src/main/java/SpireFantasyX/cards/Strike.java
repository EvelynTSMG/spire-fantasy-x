package SpireFantasyX.cards;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;

public class Strike extends BaseCard {
    public final static String NAME = Strike.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.BASIC;

    public final static int COST = 1;
    public final static int DMG = 6;
    public final static int UP_DMG = 3;

    public Strike() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        tags.add(CardTags.STARTER_STRIKE);
        tags.add(CardTags.STRIKE);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        damage(m, AbstractGameAction.AttackEffect.SLASH_DIAGONAL);
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
    }
}