package SpireFantasyX.cards;

import SpireFantasyX.actions.CompareAction;
import SpireFantasyX.powers.JinxPower;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_enemy_now;
import static SpireFantasyX.util.Wiz.atb;

public class AbaddonFlame extends BaseCard {
    public final static String NAME = AbaddonFlame.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 2;
    public final static int UP_COST = 1;
    public final static int DMG = 7;
    public final static int UP_DMG = 4;

    public AbaddonFlame() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new CompareAction<>(
            new DamageAction(m, new DamageInfo(p, damage, damageTypeForTurn), AbstractGameAction.AttackEffect.FIRE),
            () -> m.currentHealth,
            (old_health) -> apply_enemy_now(m, new JinxPower(m, old_health - m.currentHealth))
        ));
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
        upgradeBaseCost(UP_COST);
        updateDescription();
    }
}