package SpireFantasyX.cards;

import SpireFantasyX.powers.FocusedPower;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_self;

public class Focus extends BaseCard {
    public final static String NAME = Focus.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.POWER;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 2;

    public Focus() {
        super(NAME, COST, TYPE, RARITY, TARGET);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_self(new FocusedPower(p));
    }

    @Override
    public void upp() {
        isInnate = true;
        updateDescription();
    }
}