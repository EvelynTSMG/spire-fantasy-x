package SpireFantasyX.cards;

import SpireFantasyX.actions.CompareAction;
import com.megacrit.cardcrawl.actions.common.RemoveAllBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.DexterityPower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_self_now;
import static SpireFantasyX.util.Wiz.atb;

public class PullBack extends BaseCard {
    public final static String NAME = PullBack.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.POWER;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = 3;
    public final static int UP_COST = 2;
    public final static int MAGIC = 1;
    public final static int MAGIC2 = 7;

    public PullBack() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
        magic2(MAGIC2);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new CompareAction<>(
            new RemoveAllBlockAction(p, p),
            () -> p.currentBlock,
            (old_block) -> apply_self_now(new DexterityPower(p, magicNumber * old_block / magic2))
        ));
    }

    @Override
    public void upp() {
        upgradeBaseCost(UP_COST);
        updateDescription();
    }
}