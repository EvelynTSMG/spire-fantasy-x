package SpireFantasyX.cards;

import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.VulnerablePower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_enemy;
import static SpireFantasyX.util.Wiz.apply_self;

public class Provoke extends BaseCard {
    public final static String NAME = Provoke.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 0;
    public final static int BLOCK = 10;
    public final static int MAGIC = 2;
    public final static int UP_MAGIC = -1;
    public final static int MAGIC2 = 1;
    public final static int UP_MAGIC2 = 1;

    public Provoke() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        block(BLOCK);
        magic(MAGIC);
        magic2(MAGIC2);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_self(new VulnerablePower(p, magicNumber, true));
        apply_enemy(m, new VulnerablePower(m, magic2, false));
        block();
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        upgradeMagic2(UP_MAGIC2);
        updateDescription();
    }
}