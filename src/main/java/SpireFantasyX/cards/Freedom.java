package SpireFantasyX.cards;

import SpireFantasyX.actions.InstantAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.player;

public class Freedom extends BaseCard {
    public final static String NAME = Freedom.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 2;
    public final static int UP_COST = 1;
    public final static int MAGIC = 1;

    public Freedom() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new InstantAction(() -> {
            for (AbstractCard c : player().hand.group) {
                c.setCostForTurn(c.costForTurn - magicNumber);
            }
        }));
    }

    @Override
    public void upgradeName() {
        ++this.timesUpgraded;
        this.upgraded = true;
        this.name = CARD_STRINGS.EXTENDED_DESCRIPTION[0];
        this.initializeTitle();
    }

    @Override
    public void upp() {
        upgradeBaseCost(UP_COST);
        updateDescription();
    }
}