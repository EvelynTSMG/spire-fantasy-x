package SpireFantasyX.cards;

import SpireFantasyX.SpireFantasyXCharacter;
import SpireFantasyX.util.CardArtRoller;
import SpireFantasyX.util.ImageHelper;
import basemod.abstracts.CustomCard;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.DamageAllEnemiesAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.helpers.CardLibrary;
import com.megacrit.cardcrawl.localization.CardStrings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.SpireFantasyXMod.imagePath;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.att;
import static com.badlogic.gdx.graphics.GL20.GL_DST_COLOR;
import static com.badlogic.gdx.graphics.GL20.GL_ZERO;

public abstract class BaseCard extends CustomCard {

    protected final CardStrings CARD_STRINGS;

    public int magic2;
    public int base_magic2;
    public boolean upp_magic2;
    public boolean is_magic2_modified;

    public int dmg2;
    public int base_dmg2;
    public boolean upp_dmg2;
    public boolean is_dmg2_modified;

    public int block2;
    public int base_block2;
    public boolean upp_block2;
    public boolean is_block2_modified;

    private boolean needs_art_refresh = false;

    public BaseCard(final String name, final int cost, final CardType type, final CardRarity rarity, final CardTarget target) {
        this(name, cost, type, rarity, target, SpireFantasyXCharacter.Enums.SPIRE_FANTASY_X_COLOR);
    }

    public BaseCard(final String name, final int cost, final CardType type, final CardRarity rarity, final CardTarget target, final CardColor color) {
        super(id(name), "", card_image_path(id(name), type),
                cost, "", type, color, rarity, target);
        CARD_STRINGS = CardCrawlGame.languagePack.getCardStrings(this.cardID);
        rawDescription = CARD_STRINGS.DESCRIPTION;
        this.name = originalName = CARD_STRINGS.NAME;
        initializeTitle();
        initializeDescription();

        if (textureImg.contains("ui/missing.png")) {
            if (CardLibrary.cards != null && !CardLibrary.cards.isEmpty()) {
                CardArtRoller.computeCard(this);
            } else
                needs_art_refresh = true;
        }
    }

    public static String card_image_path(final String name, final AbstractCard.CardType type) {
        String textureString;

        switch (type) {
            case ATTACK:
            case POWER:
            case SKILL:
                textureString = imagePath("cards/" + name + ".png");
                break;
            default:
                textureString = imagePath("ui/missing.png");
                break;
        }

        FileHandle h = Gdx.files.internal(textureString);
        if (!h.exists()) {
            textureString = imagePath("ui/missing.png");
        }
        return textureString;
    }

    @Override
    public void applyPowers() {
        super.applyPowers();
        if (base_dmg2 > -1) {
            int og_base_dmg = baseDamage;
            int og_dmg = damage;
            boolean og_is_dmg_mod = isDamageModified;

            baseDamage = base_dmg2;
            super.applyPowers();
            is_dmg2_modified = (dmg2 != base_dmg2);
            dmg2 = damage;

            baseDamage = og_base_dmg;
            damage = og_dmg;
            isDamageModified = og_is_dmg_mod;
        }
    }

    @Override
    public void applyPowersToBlock() {
        super.applyPowersToBlock();
        if (base_block2 > -1) {
            int og_base_block = baseBlock;
            int og_block = block;
            boolean og_is_block_mod = isBlockModified;

            baseBlock = base_block2;
            super.applyPowersToBlock();
            is_block2_modified = (block2 != base_block2);
            block2 = block;

            baseBlock = og_base_block;
            block = og_block;
            isBlockModified = og_is_block_mod;
        }
    }

    @Override
    public void calculateCardDamage(AbstractMonster mo) {
        if (base_dmg2 > -1) {
            dmg2 = base_dmg2;

            int tmp = baseDamage;
            baseDamage = base_dmg2;

            super.calculateCardDamage(mo);

            dmg2 = damage;
            baseDamage = tmp;

            super.calculateCardDamage(mo);

            is_dmg2_modified = (dmg2 != base_dmg2);
        } else super.calculateCardDamage(mo);
    }

    @Override
    public void resetAttributes() {
        super.resetAttributes();
        magic2 = base_magic2;
        is_magic2_modified = false;

        dmg2 = base_dmg2;
        is_dmg2_modified = false;

        block2 = base_block2;
        is_block2_modified = false;
    }

    @Override
    public void displayUpgrades() {
        super.displayUpgrades();
        if (upp_magic2) {
            magic2 = base_magic2;
            is_magic2_modified = true;
        }
        if (upp_dmg2) {
            dmg2 = base_dmg2;
            is_dmg2_modified = true;
        }
        if (upp_block2) {
            block2 = base_block2;
            is_block2_modified = true;
        }
    }

    protected void upgradeMagic2(int amount) {
        base_magic2 += amount;
        magic2 = base_magic2;
        upp_magic2 = true;
    }

    protected void upgradeDamage2(int amount) {
        base_dmg2 += amount;
        dmg2 = base_dmg2;
        upp_dmg2 = true;
    }

    protected void upgradeBlock2(int amount) {
        base_block2 += amount;
        block2 = base_block2;
        upp_block2 = true;
    }

    protected void updateDescription() {
        if (CARD_STRINGS.UPGRADE_DESCRIPTION != null) rawDescription = CARD_STRINGS.UPGRADE_DESCRIPTION;
        initializeDescription();
    }

    @Override
    protected Texture getPortraitImage() {
        if (textureImg.contains("ui/missing.png")) {
            return CardArtRoller.getPortraitTexture(this);
        }

        // If we actually have an image to work with
        Texture unmasked = super.getPortraitImage();

        // The below masks the card art correctly no matter what.
        // Relevant masks can be found under images/masks/

        TextureAtlas.AtlasRegion t = new TextureAtlas.AtlasRegion(unmasked, 0, 0, unmasked.getWidth(), unmasked.getHeight());

        FrameBuffer fb = ImageHelper.create_buffer(500, 380);
        OrthographicCamera og = new OrthographicCamera(500, 380);
        SpriteBatch sb = new SpriteBatch();
        sb.setProjectionMatrix(og.combined);
        ImageHelper.begin_buffer(fb);

        sb.begin();
        sb.draw(t, -250, -190);

        sb.setBlendFunction(GL_DST_COLOR, GL_ZERO);
        Texture mask = CardArtRoller.get_mask(this);
        sb.setProjectionMatrix(new OrthographicCamera(500, 380).combined);
        sb.draw(mask, -250, -190, -250, -190, 500, 380, 1, 1, 0, 0, 0, mask.getWidth(), mask.getHeight(), false, true);

        sb.end();
        fb.end();

        TextureRegion masked = ImageHelper.buffer_texture(fb);
        return masked.getTexture();
    }

    public void upgrade() {
        if (canUpgrade()) {
            upgradeName();
            upp();
        }
    }

    public abstract void upp();

    @Override
    public void update() {
        super.update();
        if (needs_art_refresh) {
            CardArtRoller.computeCard(this);
        }
    }

    // Setters
    public final void dmg(int dmg) {
        baseDamage = damage = dmg;
    }

    public final void dmg2(int dmg2) {
        base_dmg2 = this.dmg2 = dmg2;
    }

    public final void block(int block) {
        baseBlock = this.block = block;
    }

    public final void block2(int block2) {
        base_block2 = this.block2 = block2;
    }

    public final void magic(int magic)  {
        baseMagicNumber = magicNumber = magic;
    }

    public final void magic2(int magic2) {
        base_magic2 = this.magic2 = magic2;
    }

    // These shortcuts are specifically for cards. All other shortcuts that aren't specifically for cards can go in Wiz.
    protected void damage(AbstractMonster target, AbstractGameAction.AttackEffect fx) {
        atb(new DamageAction(target, new DamageInfo(AbstractDungeon.player, damage, damageTypeForTurn), fx));
    }

    protected void damage_now(AbstractMonster target, AbstractGameAction.AttackEffect fx) {
        att(new DamageAction(target, new DamageInfo(AbstractDungeon.player, damage, damageTypeForTurn), fx));
    }

    protected void damage_all(AbstractGameAction.AttackEffect fx) {
        atb(new DamageAllEnemiesAction(AbstractDungeon.player, multiDamage, damageTypeForTurn, fx));
    }

    protected void damage_all_now(AbstractGameAction.AttackEffect fx) {
        att(new DamageAllEnemiesAction(AbstractDungeon.player, multiDamage, damageTypeForTurn, fx));
    }

    protected void damage2(AbstractMonster target, AbstractGameAction.AttackEffect fx) {
        atb(new DamageAction(target, new DamageInfo(AbstractDungeon.player, dmg2, damageTypeForTurn), fx));
    }

    protected void damage2_now(AbstractMonster target, AbstractGameAction.AttackEffect fx) {
        att(new DamageAction(target, new DamageInfo(AbstractDungeon.player, dmg2, damageTypeForTurn), fx));
    }

    protected void block() {
        atb(new GainBlockAction(AbstractDungeon.player, AbstractDungeon.player, block));
    }

    public String card_art_copy() {
        return null;
    }

    public CardArtRoller.ReskinInfo reskin_info(String ID) {
        return null;
    }
}
