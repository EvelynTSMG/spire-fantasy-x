package SpireFantasyX.cards;

import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.alive_monsters;
import static SpireFantasyX.util.Wiz.atb;

public class Guard extends BaseCard {
    public final static String NAME = Guard.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int COST = 1;
    public final static int BLOCK = 5;
    public final static int UP_BLOCK = 3;
    public final static int MAGIC = 2;
    public final static int UP_MAGIC = 1;

    public Guard() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        block(BLOCK);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        block();
        for (int i = alive_monsters().size(); i > 0; i--) {
            atb(new GainBlockAction(p, p, magicNumber));
        }
    }

    @Override
    public void upgradeName() {
        ++this.timesUpgraded;
        this.upgraded = true;
        this.name = CARD_STRINGS.EXTENDED_DESCRIPTION[0];
        this.initializeTitle();
    }

    @Override
    public void upp() {
        upgradeBlock(UP_BLOCK);
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}