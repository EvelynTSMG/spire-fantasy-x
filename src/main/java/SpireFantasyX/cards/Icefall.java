package SpireFantasyX.cards;

import SpireFantasyX.actions.InstantAction;
import SpireFantasyX.actions.ModalChoiceAction;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import java.util.ArrayList;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.att;
import static SpireFantasyX.util.Wiz.color_hex;

public class Icefall extends BaseCard {
    public final static String NAME = Icefall.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 2;

    public final static int DMG = 15;
    public final static int UP_DMG = 6;
    public final static int DMG2 = 7;

    public final static int BLOCK = 13;
    public final static int UP_BLOCK = 6;
    public final static int BLOCK2 = 7;

    public Icefall() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        dmg2(DMG2);
        block(BLOCK);
        block2(BLOCK2);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new InstantAction(() -> {
            ModalChoiceCard mostly_damage = new ModalChoiceCard(
                CARD_STRINGS.EXTENDED_DESCRIPTION[0],
                String.format(
                    CARD_STRINGS.EXTENDED_DESCRIPTION[1],
                    isDamageModified ? color_hex(Settings.GREEN_TEXT_COLOR):"", isDamageModified ? damage : baseDamage,
                    is_block2_modified ? color_hex(Settings.GREEN_TEXT_COLOR):"", is_block2_modified ? block2 : base_block2),
                () -> {
                    damage_now(m, AbstractGameAction.AttackEffect.SLASH_HEAVY);
                    att(new GainBlockAction(p, block2));
                });

            ModalChoiceCard mostly_block = new ModalChoiceCard(
                CARD_STRINGS.EXTENDED_DESCRIPTION[2],
                String.format(
                    CARD_STRINGS.EXTENDED_DESCRIPTION[3],
                    is_dmg2_modified ? color_hex(Settings.GREEN_TEXT_COLOR):"", is_dmg2_modified ? dmg2 : base_dmg2,
                    isBlockModified ? color_hex(Settings.GREEN_TEXT_COLOR):"", isBlockModified ? block : baseBlock),
                () -> {
                    damage2_now(m, AbstractGameAction.AttackEffect.SLASH_DIAGONAL);
                    att(new GainBlockAction(p, block));
                });

            ArrayList<AbstractCard> choices = new ArrayList<>();
            choices.add(mostly_damage);
            choices.add(mostly_block);

            att(new ModalChoiceAction(choices, 1,
                    CardCrawlGame.languagePack.getUIString(id("Choice")).TEXT[0]));
        }));
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
        upgradeBlock(UP_BLOCK);
        updateDescription();
    }
}