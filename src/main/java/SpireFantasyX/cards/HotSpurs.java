package SpireFantasyX.cards;

import SpireFantasyX.actions.InstantAction;
import com.evacipated.cardcrawl.mod.stslib.powers.interfaces.NonStackablePower;
import com.megacrit.cardcrawl.actions.common.ReducePowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.AbstractPower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.att;

public class HotSpurs extends BaseCard {
    public final static String NAME = HotSpurs.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = 1;
    public final static int MAGIC = 1;
    public final static int UP_MAGIC = 1;

    public HotSpurs() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
        exhaust = true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new InstantAction(() -> {
             for (AbstractPower pow : p.powers) {
                 if (pow.type == AbstractPower.PowerType.BUFF) {
                     // Best we'll get?
                     // If it may be unstackable and can't stack with itself
                     if (pow instanceof NonStackablePower && !((NonStackablePower) pow).isStackable(pow)) continue;
                     pow.stackPower(magicNumber);
                     pow.updateDescription();
                 } else if (pow.type == AbstractPower.PowerType.DEBUFF) {
                     att(new ReducePowerAction(p, p, pow, magicNumber));
                 }
             }
        }));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}