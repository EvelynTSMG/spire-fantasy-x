package SpireFantasyX.cards;

import SpireFantasyX.actions.BranchingAction;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.is_attack;

public class Dismiss extends BaseCard {
    public final static String NAME = Dismiss.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int DMG = 2;
    public final static int UP_DMG = 3;
    public final static int BLOCK = 8;
    public final static int UP_BLOCK = 4;

    public Dismiss() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        block(BLOCK);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        damage(m, AbstractGameAction.AttackEffect.BLUNT_LIGHT);
        atb(new BranchingAction(
            () -> is_attack(m.intent),
            new GainBlockAction(p, block)
        ));
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
        upgradeBlock(UP_BLOCK);
        updateDescription();
    }
}