package SpireFantasyX.cards;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.GainEnergyAction;
import com.megacrit.cardcrawl.actions.unique.LoseEnergyAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;

public class Osmose extends BaseCard {
    public final static String NAME = Osmose.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int COST = 0;
    public final static int UP_COST = 1;
    public final static int DMG = 8;
    public final static int UP_DMG = 3;
    public final static int MAGIC = 1;

    public Osmose() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        damage(m, AbstractGameAction.AttackEffect.SLASH_HORIZONTAL);
        if (upgraded) atb(new GainEnergyAction(magicNumber));
        else atb(new LoseEnergyAction(magicNumber));
    }

    @Override
    public void upp() {
        upgradeBaseCost(UP_COST);
        upgradeDamage(UP_DMG);
        updateDescription();
    }
}