package SpireFantasyX.cards;

import SpireFantasyX.actions.BranchingAction;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.FlightPower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;

public class MeteorStrike extends BaseCard {
    public final static String NAME = MeteorStrike.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int COST = 1;
    public final static int DMG = 7;
    public final static int UP_DMG = 4;

    public MeteorStrike() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new BranchingAction(
            () -> m.hasPower(FlightPower.POWER_ID),
            new DamageAction(m, new DamageInfo(p, 2*damage, damageTypeForTurn), AbstractGameAction.AttackEffect.FIRE),
            new DamageAction(m, new DamageInfo(p, damage, damageTypeForTurn), AbstractGameAction.AttackEffect.FIRE)
        ));
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
        updateDescription();
    }
}