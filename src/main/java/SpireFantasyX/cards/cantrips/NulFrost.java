package SpireFantasyX.cards.cantrips;

import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;

public class NulFrost extends BaseCantripCard {
    public final static String NAME = NulFrost.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int BLOCK = 3;

    public NulFrost() {
        super(NAME, TYPE, RARITY, TARGET);
        block(BLOCK);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        block();
        super.use(p, m);
    }
}