package SpireFantasyX.cards.cantrips;

import SpireFantasyX.powers.JinxPower;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_enemy;

public class NulShock extends BaseCantripCard {
    public final static String NAME = NulShock.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int MAGIC = 2;

    public NulShock() {
        super(NAME, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_enemy(m, new JinxPower(m, magicNumber));
        super.use(p, m);
    }
}