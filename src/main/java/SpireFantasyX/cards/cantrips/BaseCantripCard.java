package SpireFantasyX.cards.cantrips;

import SpireFantasyX.cards.BaseCard;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.util.Wiz.atb;

public abstract class BaseCantripCard extends BaseCard {
    public BaseCantripCard(String name, CardType type, CardRarity rarity, CardTarget target) {
        super(name, 0, type, rarity, target);
    }

    @Override
    public boolean freeToPlay() {
        costForTurn = 0;
        isCostModifiedForTurn = false;
        return true;
    }

    @Override
    public void upgradeName() {
        ++this.timesUpgraded;
        this.upgraded = true;
        this.initializeTitle();
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new DrawCardAction(1));
    }

    @Override
    public boolean canUpgrade() { return false; }

    @Override
    public void upp() { }
}
