package SpireFantasyX.cards;

import SpireFantasyX.powers.MultiOf9Power;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_self;

public class QuartetOf9 extends BaseCard {
    public final static String NAME = QuartetOf9.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int UP_COST = 0;
    public final static int MAGIC = 4;
    public final static int MAGIC2 = 9;

    public QuartetOf9() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
        magic2(MAGIC2);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_self(new MultiOf9Power(p, magicNumber, magic2));
    }

    @Override
    public void upp() {
        upgradeBaseCost(UP_COST);
        if (timesUpgraded > 1) upgradeMagic2(timesUpgraded);
        updateDescription();
    }
}