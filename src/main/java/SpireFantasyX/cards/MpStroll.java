package SpireFantasyX.cards;

import SpireFantasyX.powers.MpStrollPower;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_self;

public class MpStroll extends BaseCard {
    public final static String NAME = MpStroll.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 2;
    public final static int UP_COST = 1;
    public final static int MAGIC = 1;
    public final static int MAGIC2 = 3;

    public MpStroll() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
        magic2(MAGIC2);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_self(new MpStrollPower(p, magic2, magicNumber));
    }

    @Override
    public void upp() {
        upgradeBaseCost(UP_COST);
        updateDescription();
    }
}