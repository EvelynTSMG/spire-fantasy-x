package SpireFantasyX.cards;

import SpireFantasyX.actions.BranchingAction;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.is_attack;

public class Copycat extends BaseCard {
    public final static String NAME = Copycat.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int COST = 1;
    public final static int DMG = 10;
    public final static int UP_DMG = 4;
    public final static int BLOCK = 8;
    public final static int UP_BLOCK = 5;

    public Copycat() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        block(BLOCK);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new BranchingAction(
            () -> is_attack(m.intent),
            new DamageAction(m,
                new DamageInfo(p, damage, damageTypeForTurn),
                AbstractGameAction.AttackEffect.SLASH_DIAGONAL
            ),
            new GainBlockAction(p, block)
        ));
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
        upgradeBlock(UP_BLOCK);
        updateDescription();
    }
}