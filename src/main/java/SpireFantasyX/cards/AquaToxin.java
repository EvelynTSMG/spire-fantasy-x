package SpireFantasyX.cards;

import SpireFantasyX.actions.BranchingAction;
import SpireFantasyX.actions.CompareAction;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.PoisonPower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.att;

public class AquaToxin extends BaseCard {
    public final static String NAME = AquaToxin.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int MAGIC = 2;
    public final static int UP_MAGIC = 1;

    public AquaToxin() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new BranchingAction(() -> m.hasPower(PoisonPower.POWER_ID),
            new CompareAction<>(
                new RemoveSpecificPowerAction(m, p, PoisonPower.POWER_ID),
                () -> m.getPower(PoisonPower.POWER_ID).amount,
                amount -> {
                    att(new DamageAction(m,
                        new DamageInfo(p, amount * magicNumber, damageTypeForTurn),
                        AbstractGameAction.AttackEffect.POISON));
                }
            ))
        );
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}