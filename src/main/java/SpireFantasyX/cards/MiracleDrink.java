package SpireFantasyX.cards;

import SpireFantasyX.actions.InstantAction;
import SpireFantasyX.powers.JinxPower;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.alive_monsters;
import static SpireFantasyX.util.Wiz.apply_enemy_now;
import static SpireFantasyX.util.Wiz.atb;

public class MiracleDrink extends BaseCard {
    public final static String NAME = MiracleDrink.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.ALL_ENEMY;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int COST = 1;
    public final static int MAGIC = 3;
    public final static int UP_MAGIC = 4;

    public MiracleDrink() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
        exhaust = true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new InstantAction(() -> {
            for (AbstractMonster mon : alive_monsters()) {
                apply_enemy_now(mon, new JinxPower(mon, magicNumber));
            }
        }));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        exhaust = false;
        isEthereal = true;
        updateDescription();
    }
}