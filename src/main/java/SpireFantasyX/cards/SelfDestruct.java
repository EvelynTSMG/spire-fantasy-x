package SpireFantasyX.cards;

import SpireFantasyX.powers.SelfDestructPower;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.alive_monsters;
import static SpireFantasyX.util.Wiz.apply_self;

public class SelfDestruct extends BaseCard {
    public final static String NAME = SelfDestruct.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.POWER;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = 1;
    public final static int UP_COST = 0;
    public final static int MAGIC = 5;
    public final static int MAGIC2 = 1;

    public SelfDestruct() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
        magic2(MAGIC2);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_self(new SelfDestructPower(p, magicNumber, magic2, upgraded));
    }

    @Override
    public void upp() {
        upgradeBaseCost(UP_COST);
        updateDescription();
    }
}