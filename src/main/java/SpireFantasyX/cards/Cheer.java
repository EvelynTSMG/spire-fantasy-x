package SpireFantasyX.cards;

import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.DexterityPower;
import com.megacrit.cardcrawl.powers.LoseDexterityPower;
import com.megacrit.cardcrawl.powers.LoseStrengthPower;
import com.megacrit.cardcrawl.powers.StrengthPower;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_self;

public class Cheer extends BaseCard {
    public final static String NAME = Cheer.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.BASIC;

    public final static int COST = 0;
    public final static int MAGIC = 2;

    public Cheer() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_self(new StrengthPower(p, magicNumber));
        apply_self(new LoseStrengthPower(p, magicNumber));

        if (upgraded) {
            apply_self(new DexterityPower(p, magicNumber));
            apply_self(new LoseDexterityPower(p, magicNumber));
        }
    }

    @Override
    public void upp() {
        updateDescription();
    }
}