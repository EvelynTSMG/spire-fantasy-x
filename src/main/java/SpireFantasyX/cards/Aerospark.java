package SpireFantasyX.cards;

import SpireFantasyX.actions.InstantAction;
import SpireFantasyX.powers.AerosparkPower;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.apply_self;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.att;

public class Aerospark extends BaseCard {
    public final static String NAME = Aerospark.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.NONE;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int COST = 2;
    public final static int BLOCK = 5;
    public final static int MAGIC = 2;
    public final static int UP_MAGIC = 1;

    public Aerospark() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        block(BLOCK);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new DrawCardAction(magicNumber, new InstantAction(() -> {
            for (long i = DrawCardAction.drawnCards.stream().filter(c -> c.type == CardType.SKILL).count(); i > 0; i--)
                att(new GainBlockAction(p, block));
        })));
        if (upgraded) apply_self(new AerosparkPower(p, block));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}