package SpireFantasyX;

import SpireFantasyX.cards.Cheer;
import SpireFantasyX.cards.Defend;
import SpireFantasyX.cards.Strike;
import SpireFantasyX.cards.magic.Thunder;
import SpireFantasyX.relics.FlappyHood;
import basemod.abstracts.CustomEnergyOrb;
import basemod.abstracts.CustomPlayer;
import basemod.animations.SpriterAnimation;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.MathUtils;
import com.evacipated.cardcrawl.modthespire.lib.SpireEnum;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.core.EnergyManager;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.CardLibrary;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.helpers.ScreenShake;
import com.megacrit.cardcrawl.localization.CharacterStrings;
import com.megacrit.cardcrawl.screens.CharSelectInfo;

import java.util.ArrayList;

import static SpireFantasyX.SpireFantasyXCharacter.Enums.SPIRE_FANTASY_X_COLOR;
import static SpireFantasyX.SpireFantasyXMod.CORPSE;
import static SpireFantasyX.SpireFantasyXMod.SHOULDER1;
import static SpireFantasyX.SpireFantasyXMod.SHOULDER2;
import static SpireFantasyX.SpireFantasyXMod.charPath;
import static SpireFantasyX.SpireFantasyXMod.char_color;
import static SpireFantasyX.SpireFantasyXMod.id;

public class SpireFantasyXCharacter extends CustomPlayer {

    static final String ID = id("SpireFantasyXCharacter");
    static final CharacterStrings characterStrings = CardCrawlGame.languagePack.getCharacterString(ID);
    static final String[] NAMES = characterStrings.NAMES;
    static final String[] TEXT = characterStrings.TEXT;
    public static final int HP = 84;
    public static final int ORB_SLOTS = 0;
    public static final int GOLD = 99;
    public static final int CARD_DRAW = 5;


    public SpireFantasyXCharacter(String name, PlayerClass setClass) {
        super(name, setClass, new CustomEnergyOrb(orbTextures, charPath("mainChar/orb/vfx.png"), null), new SpriterAnimation(
                charPath("mainChar/static.scml")));
        initializeClass(null,
                SHOULDER1,
                SHOULDER2,
                CORPSE,
                getLoadout(), 20.0F, -10.0F, 166.0F, 327.0F, new EnergyManager(3));


        dialogX = (drawX + 0.0F * Settings.scale);
        dialogY = (drawY + 240.0F * Settings.scale);
    }

    @Override
    public CharSelectInfo getLoadout() {
        return new CharSelectInfo(NAMES[0], TEXT[0],
                HP, HP, ORB_SLOTS, GOLD, CARD_DRAW, this, getStartingRelics(),
                getStartingDeck(), false);
    }

    @Override
    public ArrayList<String> getStartingDeck() {
        ArrayList<String> deck = new ArrayList<>();

        for (int i = 0; i < 4; i++) deck.add(Strike.ID);
        deck.add(Thunder.ID);

        for (int i = 0; i < 4; i++) deck.add(Defend.ID);
        deck.add(Cheer.ID);

        return deck;
    }

    public ArrayList<String> getStartingRelics() {
        ArrayList<String> relics = new ArrayList<>();
        relics.add(FlappyHood.ID);
        return relics;
    }

    @Override
    public void doCharSelectScreenSelectEffect() {
        CardCrawlGame.sound.playA("UNLOCK_PING", MathUtils.random(-0.2F, 0.2F));
        CardCrawlGame.screenShake.shake(ScreenShake.ShakeIntensity.LOW, ScreenShake.ShakeDur.SHORT,
                false);
    }

    private static final String[] orbTextures = {
            charPath("mainChar/orb/layer1.png"),
            charPath("mainChar/orb/layer2.png"),
            charPath("mainChar/orb/layer3.png"),
            charPath("mainChar/orb/layer4.png"),
            charPath("mainChar/orb/layer4.png"),
            charPath("mainChar/orb/layer6.png"),
            charPath("mainChar/orb/layer1d.png"),
            charPath("mainChar/orb/layer2d.png"),
            charPath("mainChar/orb/layer3d.png"),
            charPath("mainChar/orb/layer4d.png"),
            charPath("mainChar/orb/layer5d.png"),
    };

    @Override
    public String getCustomModeCharacterButtonSoundKey() {
        return "UNLOCK_PING";
    }

    @Override
    public int getAscensionMaxHPLoss() {
        return 8;
    }

    @Override
    public AbstractCard.CardColor getCardColor() {
        return SPIRE_FANTASY_X_COLOR;
    }

    @Override
    public Color getCardTrailColor() {
        return char_color.cpy();
    }

    @Override
    public BitmapFont getEnergyNumFont() {
        return FontHelper.energyNumFontRed;
    }

    @Override
    public String getLocalizedCharacterName() {
        return NAMES[0];
    }

    @Override
    public AbstractCard getStartCardForEvent() {
        return new Cheer();
    }

    @Override
    public String getTitle(AbstractPlayer.PlayerClass playerClass) {
        return NAMES[1];
    }

    @Override
    public AbstractPlayer newInstance() {
        return new SpireFantasyXCharacter(name, chosenClass);
    }

    @Override
    public Color getCardRenderColor() {
        return char_color.cpy();
    }

    @Override
    public Color getSlashAttackColor() {
        return char_color.cpy();
    }

    @Override
    public AbstractGameAction.AttackEffect[] getSpireHeartSlashEffect() {
        return new AbstractGameAction.AttackEffect[]{
                AbstractGameAction.AttackEffect.FIRE,
                AbstractGameAction.AttackEffect.BLUNT_HEAVY,
                AbstractGameAction.AttackEffect.FIRE};
    }

    @Override
    public String getSpireHeartText() {
        return TEXT[1];
    }

    @Override
    public String getVampireText() {
        return TEXT[2];
    }

    public static class Enums {
        @SpireEnum
        public static AbstractPlayer.PlayerClass SPIRE_FANTASY_X_CLASS;
        @SpireEnum(name = "SPIRE_FANTASY_X_COLOR")
        public static AbstractCard.CardColor SPIRE_FANTASY_X_COLOR;
        @SpireEnum(name = "SPIRE_FANTASY_X_COLOR")
        @SuppressWarnings("unused")
        public static CardLibrary.LibraryType LIBRARY_COLOR;
    }
}
