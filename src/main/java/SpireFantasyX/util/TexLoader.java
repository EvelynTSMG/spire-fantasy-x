package SpireFantasyX.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.evacipated.cardcrawl.modthespire.lib.SpirePatch;
import com.evacipated.cardcrawl.modthespire.lib.SpirePrefixPatch;

import java.util.HashMap;

import static SpireFantasyX.SpireFantasyXMod.imagePath;

public class TexLoader {
    private static HashMap<String, Texture> textures = new HashMap<>();

    /**
     * @param imagePath - String path to the texture you want to load relative to resources,<br>
     *                      Example: imagePath("missing.png")
     * @return <b>com.badlogic.gdx.graphics.Texture</b> - The texture from the path provided
     */
    public static Texture get_texture(final String imagePath) {
        if (textures.get(imagePath) == null) {
            try {
                load_texture(imagePath, true);
            } catch (GdxRuntimeException e) {
                return get_texture(imagePath("ui/missing.png"));
            }
        }
        return textures.get(imagePath);
    }

    private static void load_texture(final String imagePath) throws GdxRuntimeException {
        load_texture(imagePath, false);
    }

    private static void load_texture(final String imagePath, boolean linearFilter) throws GdxRuntimeException {
        Texture texture = new Texture(imagePath);
        if (linearFilter) {
            texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        } else {
            texture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        }
        textures.put(imagePath, texture);
    }

    public static boolean does_texture_exist(String filepath) {
        return Gdx.files.internal(filepath).exists();
    }

    public static TextureAtlas.AtlasRegion texture_as_atlas_region(String textureString) {
        Texture texture = get_texture(textureString);
        return ImageHelper.as_atlas_region(texture);
    }


    @SpirePatch(clz = Texture.class, method="dispose")
    public static class DisposeListener {
        @SpirePrefixPatch
        public static void DisposeListenerPatch(final Texture __instance) {
            textures.entrySet().removeIf(entry -> {
                if (entry.getValue().equals(__instance)) System.out.println("TextureLoader | Removing Texture: " + entry.getKey());
                return entry.getValue().equals(__instance);
            });
        }
    }

}