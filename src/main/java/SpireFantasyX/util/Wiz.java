package SpireFantasyX.util;

import SpireFantasyX.SpireFantasyXMod;
import SpireFantasyX.actions.TimedVFXAction;
import com.badlogic.gdx.graphics.Color;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.animations.VFXAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.DiscardAction;
import com.megacrit.cardcrawl.actions.common.MakeTempCardInDrawPileAction;
import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardGroup;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.helpers.CardLibrary;
import com.megacrit.cardcrawl.helpers.Hitbox;
import com.megacrit.cardcrawl.helpers.input.InputHelper;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.megacrit.cardcrawl.random.Random;
import com.megacrit.cardcrawl.rooms.AbstractRoom;
import com.megacrit.cardcrawl.vfx.AbstractGameEffect;

import java.util.ArrayList;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Wiz {
    public static AbstractPlayer player() {
        return AbstractDungeon.player;
    }

    public static AbstractRoom room() {
        return AbstractDungeon.getCurrRoom();
    }

    /**
     * Run a lambda on all cards in cardsList.
     * @deprecated
     * Write a for loop manually.
     */
    @Deprecated
    public static void forAllCardsInList(Consumer<AbstractCard> consumer, ArrayList<AbstractCard> cardsList) {
        for (AbstractCard c : cardsList) {
            consumer.accept(c);
        }
    }

    /**
     * Ignores CARD_POOL and UNSPECIFIED card group types.
     *
     * @param groups The card groups to get cards from
     * @return The cards from the specified groups
     */
    public static ArrayList<AbstractCard> cards_in_groups(CardGroup.CardGroupType... groups) {
        ArrayList<AbstractCard> cards = new ArrayList<>();

        for (CardGroup.CardGroupType group : groups) {
            switch (group) {
                case MASTER_DECK:
                    cards.addAll(player().masterDeck.group);
                    break;
                case DRAW_PILE:
                    cards.addAll(player().drawPile.group);
                    break;
                case DISCARD_PILE:
                    cards.addAll(player().discardPile.group);
                    break;
                case EXHAUST_PILE:
                    cards.addAll(player().exhaustPile.group);
                    break;
                case HAND:
                    cards.addAll(player().hand.group);
                    break;
            }
        }

        return cards;
    }

    /**
     * Run a lambda on all alive monsters.
     * @deprecated
     * Write a for loop manually.
     */
    @Deprecated
    public static void forAllMonstersLiving(Consumer<AbstractMonster> consumer) {
        for (AbstractMonster m : alive_monsters()) {
            consumer.accept(m);
        }
    }

    public static ArrayList<AbstractMonster> alive_monsters() {
        ArrayList<AbstractMonster> monsters = new ArrayList<>(AbstractDungeon.getMonsters().monsters);
        monsters.removeIf(m -> m.isDead || m.isDying);
        return monsters;
    }

    public static ArrayList<AbstractCard> cards(Predicate<AbstractCard> filter) {
        return cards(filter, false);
    }

    public static ArrayList<AbstractCard> cards(Predicate<AbstractCard> filter, boolean from_all_cards) {
        ArrayList<AbstractCard> cards = new ArrayList<>();

        if (from_all_cards) {
            for (AbstractCard c : CardLibrary.getAllCards()) {
                if (filter.test(c)) cards.add(c.makeStatEquivalentCopy());
            }
        } else {
            for (AbstractCard c : AbstractDungeon.srcCommonCardPool.group) {
                if (filter.test(c)) cards.add(c.makeStatEquivalentCopy());
            }
            for (AbstractCard c : AbstractDungeon.srcUncommonCardPool.group) {
                if (filter.test(c)) cards.add(c.makeStatEquivalentCopy());
            }
            for (AbstractCard c : AbstractDungeon.srcRareCardPool.group) {
                if (filter.test(c)) cards.add(c.makeStatEquivalentCopy());
            }
        }

        return cards;
    }

    public static Optional<AbstractCard> random_card(Predicate<AbstractCard> filter, boolean from_all_cards) {
        return random_item(cards(filter, from_all_cards));
    }

    public static Optional<AbstractCard> random_card(Predicate<AbstractCard> filter) {
        return random_card(filter, false);
    }

    /**
     * Ignores CARD_POOL and UNSPECIFIED card group types.
     *
     * @param filter Filter to apply to the cards
     * @param groups The card groups to get cards from
     * @return A random card from the specified groups, or Empty if all the groups were empty
     */
    public static Optional<AbstractCard> random_card_from(Predicate<AbstractCard> filter, CardGroup.CardGroupType... groups) {
        return random_item(cards_in_groups(groups)
                .stream()
                .filter(filter)
                .collect(Collectors.toCollection(ArrayList::new)));
    }

    public static <T> Optional<T> random_item(ArrayList<T> from, Random rng) {
        return from.isEmpty() ? Optional.empty() : Optional.ofNullable(from.get(rng.random(from.size() - 1)));
    }

    public static <T> Optional<T> random_item(ArrayList<T> from) {
        return random_item(from, AbstractDungeon.cardRandomRng);
    }

    private static boolean is_hovered(Hitbox hb) {
        return InputHelper.mX > hb.x
            && InputHelper.mX < hb.x + hb.width
            && InputHelper.mY > hb.y
            && InputHelper.mY < hb.y + hb.height;
    }

    public static boolean is_in_combat() {
        return CardCrawlGame.isInARun()
            && AbstractDungeon.currMapNode != null
            && AbstractDungeon.getCurrRoom() != null
            && AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT;
    }

    public static void atb(AbstractGameAction action) {
        AbstractDungeon.actionManager.addToBottom(action);
    }

    public static void att(AbstractGameAction action) {
        AbstractDungeon.actionManager.addToTop(action);
    }

    public static void vfx(AbstractGameEffect effect) {
        atb(new VFXAction(effect));
    }

    public static void vfx(AbstractGameEffect effect, float duration) {
        atb(new VFXAction(effect, duration));
    }

    public static void tfx(AbstractGameEffect effect) {
        atb(new TimedVFXAction(effect));
    }

    public static void make_card_hand(AbstractCard c, int amount) {
        atb(new MakeTempCardInHandAction(c, amount));
    }

    public static void make_card_hand(AbstractCard c) {
        make_card_hand(c, 1);
    }

    public static void make_card_shuffle(AbstractCard c, int amount) {
        atb(new MakeTempCardInDrawPileAction(c, amount, true, true));
    }

    public static void make_card_shuffle(AbstractCard c) {
        make_card_shuffle(c, 1);
    }

    public static void make_card_top(AbstractCard c, int amount) {
        atb(new MakeTempCardInDrawPileAction(c, amount, false, true));
    }

    public static void make_card_top(AbstractCard c) {
        make_card_top(c, 1);
    }

    public static void apply_enemy(AbstractMonster target, AbstractPower power) {
        atb(new ApplyPowerAction(target, AbstractDungeon.player, power, power.amount));
    }

    public static void apply_enemy_now(AbstractMonster target, AbstractPower power) {
        att(new ApplyPowerAction(target, AbstractDungeon.player, power, power.amount));
    }

    public static void apply_self(AbstractPower power) {
        atb(new ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, power, power.amount));
    }

    public static void apply_self_now(AbstractPower power) {
        att(new ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, power, power.amount));
    }

    public static void thorn_dmg(AbstractCreature target, int amount, AbstractGameAction.AttackEffect atk_fx) {
        atb(new DamageAction(target, new DamageInfo(AbstractDungeon.player, amount, DamageInfo.DamageType.THORNS), atk_fx));
    }

    public static void thorn_dmg(AbstractCreature target, int amount) {
        thorn_dmg(target, amount, AbstractGameAction.AttackEffect.NONE);
    }

    public static void discard(int amount, boolean isRandom) {
        atb(new DiscardAction(player(), player(), amount, isRandom));
    }

    public static void discard(int amount) {
        discard(amount, false);
    }

    public static Optional<Integer> power_amount(AbstractCreature of, Class<? extends AbstractPower> power) {
        for (AbstractPower p : of.powers) {
            if (p.getClass().equals(power)) {
                return Optional.of(p.amount);
            }
        }

        return Optional.empty();
    }

    public static boolean is_attack(AbstractMonster.Intent intent) {
        return intent == AbstractMonster.Intent.ATTACK
            || intent == AbstractMonster.Intent.ATTACK_BUFF
            || intent == AbstractMonster.Intent.ATTACK_DEBUFF
            || intent == AbstractMonster.Intent.ATTACK_DEFEND;
    }

    public static boolean is_block(AbstractMonster.Intent intent) {
        return intent == AbstractMonster.Intent.DEFEND
            || intent == AbstractMonster.Intent.DEFEND_BUFF
            || intent == AbstractMonster.Intent.DEFEND_DEBUFF
            || intent == AbstractMonster.Intent.ATTACK_DEFEND;
    }

    public static String get_energy_string(int amount, boolean color_number) {
        switch (amount) {
            case 1: return "[E]";
            case 2: return "[E] [E]";
            case 3: return "[E] [E] [E]";
        }

        return (color_number ? "#b" : "") + amount + " [E]";
    }

    public static String char_color() {
        return color_hex(SpireFantasyXMod.char_color);
    }

    public static String color_hex(Color color) {
        return String.format("[#%x%x%x]", (int)(color.r * 255), (int)(color.g * 255), (int)(color.b * 255));
    }

    public static String color_text(String text, String color) {
        return color + text.replace(" ", " " + color);
    }
}
