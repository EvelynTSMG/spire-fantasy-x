package SpireFantasyX.relics;

import SpireFantasyX.SpireFantasyXCharacter;
import SpireFantasyX.util.TexLoader;
import basemod.abstracts.CustomRelic;
import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.SpireFantasyXMod.relicPath;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.player;

public abstract class BaseRelic extends CustomRelic {
    public CardColor color;

    public BaseRelic(String name, RelicTier tier, LandingSound sfx) {
        this(name, tier, sfx, SpireFantasyXCharacter.Enums.SPIRE_FANTASY_X_COLOR);
    }

    public BaseRelic(String name, RelicTier tier, LandingSound sfx, CardColor color) {
        super(id(name), TexLoader.get_texture(relicPath(name + ".png")), tier, sfx);
        outlineImg = TexLoader.get_texture(relicPath("outlines/" + name + ".png"));
        this.color = color;
    }

    public void flash_above_player() {
        atb(new RelicAboveCreatureAction(player(), this));
    }

    @Override
    public String getUpdatedDescription() {
        return DESCRIPTIONS[0];
    }
}
