package SpireFantasyX.relics;

import SpireFantasyX.powers.JinxPower;
import com.megacrit.cardcrawl.actions.common.ApplyPowerToRandomEnemyAction;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.relics.AbstractRelic;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.char_color;
import static SpireFantasyX.util.Wiz.color_text;
import static SpireFantasyX.util.Wiz.player;

public class SublimelyMagnificentFlappyHoodMarkIII extends BaseRelic {
    public final static String NAME = SublimelyMagnificentFlappyHoodMarkIII.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static RelicTier TIER = RelicTier.BOSS;
    public final static LandingSound SOUND = LandingSound.FLAT;
    public final static int EFFECT = 3;
    private final static String STARTER_NAME = CardCrawlGame.languagePack.getRelicStrings(FlappyHood.ID).NAME;

    public SublimelyMagnificentFlappyHoodMarkIII() {
        super(NAME, TIER, SOUND);
    }

    @Override
    public void obtain() {
        for (int i = player().relics.size() - 1; i >= 0; i--) {
            if (player().relics.get(i).relicId.equals(FlappyHood.ID)) {
                instantObtain(player(), i, true);
                return;
            }
        }

        super.obtain();
    }

    @Override
    public void atTurnStart() {
        atb(new ApplyPowerToRandomEnemyAction(player(), new JinxPower(null, EFFECT)));
    }

    @Override
    public String getUpdatedDescription() {
        return String.format(DESCRIPTIONS[0], color_text(STARTER_NAME, char_color()), EFFECT);
    }
}
