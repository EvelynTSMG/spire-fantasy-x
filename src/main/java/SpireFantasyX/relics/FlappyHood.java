package SpireFantasyX.relics;

import SpireFantasyX.powers.JinxPower;
import com.megacrit.cardcrawl.actions.common.ApplyPowerToRandomEnemyAction;

import static SpireFantasyX.SpireFantasyXMod.id;
import static SpireFantasyX.util.Wiz.atb;
import static SpireFantasyX.util.Wiz.player;

public class FlappyHood extends BaseRelic {
    public final static String NAME = FlappyHood.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static RelicTier TIER = RelicTier.STARTER;
    public final static LandingSound SOUND = LandingSound.FLAT;
    public final static int EFFECT = 1;

    public FlappyHood() {
        super(NAME, TIER, SOUND);
    }

    @Override
    public void atTurnStart() {
        atb(new ApplyPowerToRandomEnemyAction(player(), new JinxPower(null, EFFECT)));
    }

    @Override
    public String getUpdatedDescription() {
        return String.format(DESCRIPTIONS[0], EFFECT);
    }
}